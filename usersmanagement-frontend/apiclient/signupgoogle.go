package apiclient

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"

	"bitbucket.org/luisfalcao/lu_fa/usersmanagement-frontend/apiclient/apimodel"
	"github.com/sirupsen/logrus"
)

// SigninGoogleUser - calls the signup users management API endpoint
func (api *UsersManagementAPI) SigninGoogleUser(googleOAuthToken string) (*apimodel.SignupResponse, error) {

	endpoint := api.buildURL("api/auth/goognesignin")
	req, err := http.NewRequest("POST", endpoint, nil)
	if err != nil {
		logrus.WithError(err).
			WithContext(context.Background()).
			Error("Error cccured trying to create the users management API google signup request.")
		return nil, err
	}
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Authorization", fmt.Sprintf("OAuth2 %s", googleOAuthToken))

	response, err := client.Do(req)
	if err != nil && response == nil {
		logrus.WithError(err).
			WithContext(context.Background()).
			Error("Error sending google signup request to the users management API.")
		return nil, err
	}
	defer response.Body.Close()
	responseBody, err := ioutil.ReadAll(response.Body)

	if response.StatusCode >= 200 && response.StatusCode < 300 {
		signupResponse := apimodel.SignupResponse{}
		err := json.Unmarshal(responseBody, &signupResponse)
		if err != nil {
			logrus.WithError(err).
				WithContext(context.Background()).
				Error("Error parsing google signup successfully response from the users management API.")
			return nil, err
		}
		return &signupResponse, nil

	} else if response.StatusCode == http.StatusBadRequest {
		return nil, handleBadRequest(responseBody)
	} else {
		logrus.WithField("ResponseStatusCode", response.StatusCode).
			Error("Error returned from the users management API google signin request")
		return nil, errors.New("Error returned from the users management API google signup request")
	}
}
