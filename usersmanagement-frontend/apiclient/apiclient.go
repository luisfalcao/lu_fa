package apiclient

import (
	"net/http"
)

// Client - http client to make calls to external apis
var client *http.Client

// SetHTTPClient - stores the default http client
func SetHTTPClient(c *http.Client) {
	client = c
}
