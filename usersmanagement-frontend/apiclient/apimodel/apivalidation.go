package apimodel

// APIValidationError - model with the response format of the bad requests from the API
type APIValidationError struct {
	ErrorMessage string               `json:"error,omitempty"`
	Validations  []APIFieldValidation `json:"validations,omitempty"`
}

// APIFieldValidation - model with the fields validations of the bad requests responses from the API
type APIFieldValidation struct {
	Field string `json:"field,omitempty"`
	Error string `json:"error,omitempty"`
}

func (e APIValidationError) Error() string {
	return e.ErrorMessage
}
