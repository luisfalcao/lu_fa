package apimodel

// SignupRequestRequest - model used to send data to signup api call
type SignupRequestRequest struct {
	Email           string `json:"email"`
	Password        string `json:"password"`
	ConfirmPassword string `json:"confirmPassword"`
}

// SignupResponse - model that returns the content when the user is successfully created
type SignupResponse struct {
	UserID      int64  `json:"user_id"`
	AccessToken string `json:"access_token"`
}

// SigninResponse - model that returns the content when the user is successfully authenticated
type SigninResponse struct {
	AccessToken string  `json:"access_token"`
	ExpiresIn   float64 `json:"expires_in"`
	TokenType   string  `json:"token_type"`
}

// EditProfileRequest  - model used to send data to the edit profile endpoint
type EditProfileRequest struct {
	Email     string `json:"email"`
	Name      string `json:"name"`
	Telephone string `json:"telephone"`
	Address   string `json:"address"`
}
