package apimodel

import "time"

// UserResponse - mnodel with the users informations
type UserResponse struct {
	ID          int64     `json:"user_id"`
	Name        string    `json:"name"`
	Email       string    `json:"email"`
	Telephone   string    `json:"telephone"`
	Address     string    `json:"Address"`
	Createddate time.Time `json:"created_date"`
}

// RecoverPasswordRequest - recover password request
type RecoverPasswordRequest struct {
	Email      string `json:"email"`
	RecoverURL string `json:"recoverUrl"`
}

// ChangePasswordRequest - recover password request
type ChangePasswordRequest struct {
	Password        string `json:"password"`
	ConfirmPassword string `json:"confirmPassword"`
	Token           string `json:"token"`
}
