package apiclient

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"io/ioutil"
	"net/http"

	"bitbucket.org/luisfalcao/lu_fa/usersmanagement-frontend/apiclient/apimodel"
	"github.com/sirupsen/logrus"
)

// RecoverPassword - send the recover password request to the users management api
func (api *UsersManagementAPI) RecoverPassword(requestModel apimodel.RecoverPasswordRequest) error {
	body, err := json.Marshal(requestModel)
	if err != nil {
		logrus.WithError(err).
			WithContext(context.Background()).
			Error("Error occurred trying to serialize RecoverPasswordRequest struct to json.")
		return err
	}

	endpoint := api.buildURL("api/password/recover")
	req, err := http.NewRequest("POST", endpoint, bytes.NewBuffer([]byte(body)))
	if err != nil {
		logrus.WithError(err).
			WithContext(context.Background()).
			Error("Error occcured trying to create the users management API recover password request.")
		return err
	}

	response, err := client.Do(req)
	if err != nil && response == nil {
		logrus.WithError(err).
			WithContext(context.Background()).
			Error("Error sending recover password request to the users management API.")
		return err
	}
	defer response.Body.Close()
	responseBody, err := ioutil.ReadAll(response.Body)

	if response.StatusCode >= 200 && response.StatusCode < 300 {
		if err != nil {
			logrus.WithError(err).
				WithContext(context.Background()).
				Error("Error parsing recover password successfully response from the users management API.")
			return err
		}
		return nil

	} else if response.StatusCode == http.StatusBadRequest {
		return handleBadRequest(responseBody)
	} else {
		logrus.WithError(err).
			WithContext(context.Background()).
			Error("Error returned from the users management API signup request")
		return errors.New("Error returned from the users management API signup request")
	}
}

// ChangePassword - send change password request to the users management api
func (api *UsersManagementAPI) ChangePassword(requestModel apimodel.ChangePasswordRequest) error {
	body, err := json.Marshal(requestModel)
	if err != nil {
		logrus.WithError(err).
			WithContext(context.Background()).
			Error("Error occurred trying to serialize ChangePasswordRequest struct to json.")
		return err
	}

	endpoint := api.buildURL("api/password/change")
	req, err := http.NewRequest("POST", endpoint, bytes.NewBuffer([]byte(body)))
	if err != nil {
		logrus.WithError(err).
			WithContext(context.Background()).
			Error("Error occcured trying to create the users management API change password request.")
		return err
	}

	response, err := client.Do(req)
	if err != nil && response == nil {
		logrus.WithError(err).
			WithContext(context.Background()).
			Error("Error sending change password request to the users management API.")
		return err
	}
	defer response.Body.Close()
	responseBody, err := ioutil.ReadAll(response.Body)

	if response.StatusCode >= 200 && response.StatusCode < 300 {
		if err != nil {
			logrus.WithError(err).
				WithContext(context.Background()).
				Error("Error parsing change password successfully response from the users management API.")
			return err
		}
		return nil

	} else if response.StatusCode == http.StatusBadRequest {
		return handleBadRequest(responseBody)
	} else {
		logrus.WithField("ResponseStatusCode", response.StatusCode).
			Error("Error returned from the users management API signup request")
		return errors.New("Error returned from the users management API signup request")
	}
}
