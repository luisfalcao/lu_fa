package apiclient

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"strconv"
	"strings"

	"bitbucket.org/luisfalcao/lu_fa/usersmanagement-frontend/apiclient/apimodel"
	"bitbucket.org/luisfalcao/lu_fa/usersmanagement-frontend/http/auth"
	"github.com/sirupsen/logrus"
)

// NewUsersManagementAPI - creates a new instance of UsersManagementApi
func NewUsersManagementAPI(apiURL, clientID, clientSecret string) *UsersManagementAPI {
	return &UsersManagementAPI{APIUrl: apiURL, clientID: clientID, clientSecret: clientSecret}
}

// UsersManagementAPI - struct with methods to work with the users management API
type UsersManagementAPI struct {
	APIUrl, clientID, clientSecret string
}

// ErrUnauthorized - this erros will be returned when some call to management users api returns an unauthorized response
var ErrUnauthorized = errors.New("Unauthorized response from API")

// ErrInvalidGrant - error returned whem the login attempt against the API return invalid_grant error
var ErrInvalidGrant = errors.New("Invalid grant_type returned from the signin api")

// ErrUnauthorizedClientApplication - error returned whem the login attempt against the API return access_denied error. It means that the clietID or clientSecret provided is wrong
var ErrUnauthorizedClientApplication = errors.New("This application is not authoirized or provided wrong authorizion parameters to the API")

// SignupUser - calls the signup users management API endpoint
func (api *UsersManagementAPI) SignupUser(requestModel apimodel.SignupRequestRequest) (*apimodel.SignupResponse, error) {

	body, err := json.Marshal(requestModel)
	if err != nil {
		logrus.WithError(err).
			WithContext(context.Background()).
			Error("Error occurred trying to serialize SignupRequestRequest struct to json.")
		return nil, err
	}

	endpoint := api.buildURL("api/auth/signup")
	req, err := http.NewRequest("POST", endpoint, bytes.NewBuffer([]byte(body)))
	if err != nil {
		logrus.WithError(err).
			WithContext(context.Background()).
			Error("Error cccured trying to create the users management API signup request.")
		return nil, err
	}
	req.Header.Set("Content-Type", "application/json")

	response, err := client.Do(req)
	if err != nil && response == nil {
		logrus.WithError(err).
			WithContext(context.Background()).
			Error("Error sending signup request to the users management API.")
		return nil, err
	}
	defer response.Body.Close()
	responseBody, err := ioutil.ReadAll(response.Body)

	if response.StatusCode >= 200 && response.StatusCode < 300 {
		signupResponse := apimodel.SignupResponse{}
		err := json.Unmarshal(responseBody, &signupResponse)
		if err != nil {
			logrus.WithError(err).
				WithContext(context.Background()).
				Error("Error parsing signup successfully response from the users management API.")
			return nil, err
		}
		return &signupResponse, nil

	} else if response.StatusCode == http.StatusBadRequest {
		return nil, handleBadRequest(responseBody)
	} else {
		logrus.WithField("ResponseStatusCode", response.StatusCode).
			Error("Error returned from the users management API signup request")
		return nil, errors.New("Error returned from the users management API signup request")
	}
}

// GetUser - calls the profile enpoint from the api to return the users information
func (api *UsersManagementAPI) GetUser(token auth.APIToken) (*apimodel.UserResponse, error) {

	endpoint := api.buildURL("api/profile")
	req, err := http.NewRequest("GET", endpoint, nil)
	if err != nil {
		logrus.WithError(err).
			WithContext(context.Background()).
			Error("Error cccured creating the request to get user data from the users management API profile endpoint.")
		return nil, err

	}
	req.Header.Set("Content-Type", "application/json")

	req.Header.Set("Authorization", fmt.Sprintf("%s %s", token.GetTokenType(), token.Token))

	response, err := client.Do(req)
	if err != nil && response == nil {
		logrus.WithError(err).
			WithContext(context.Background()).
			Error("Error cccured trying to get user data from the users management API profile endpoint.")
		return nil, err
	}
	defer response.Body.Close()
	responseBody, err := ioutil.ReadAll(response.Body)

	if response.StatusCode >= 200 && response.StatusCode < 300 {
		var userResponse apimodel.UserResponse
		err := json.Unmarshal(responseBody, &userResponse)
		if err != nil {
			logrus.WithError(err).
				WithContext(context.Background()).
				Error("Error parsing get profile successfully response from the users management API.")
			return nil, err
		}
		return &userResponse, nil

	} else if response.StatusCode == http.StatusBadRequest {
		return nil, handleBadRequest(responseBody)
	} else if response.StatusCode == http.StatusUnauthorized {
		return nil, ErrUnauthorized
	} else {
		logrus.WithField("ResponseStatusCode", response.StatusCode).
			Error("Error returned from the users management API profile endpoint")
		return nil, errors.New("Error returned from the users management API profile endpoint")
	}
}

// SigninUser - sign user
func (api *UsersManagementAPI) SigninUser(email, password string) (*apimodel.SigninResponse, error) {
	endpoint := api.buildURL("connect/token")

	data := url.Values{}
	data.Set("grant_type", "password")
	data.Set("client_id", api.clientID)
	data.Set("client_secret", api.clientSecret)
	data.Set("username", email)
	data.Set("password", password)

	req, err := http.NewRequest(http.MethodPost, endpoint, strings.NewReader(data.Encode()))
	if err != nil {
		logrus.WithError(err).
			WithContext(context.Background()).
			Error("An error occurred creating the user sign-in request against the management API profile.")
		return nil, err
	}
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	req.Header.Set("Content-Length", strconv.Itoa(len(data.Encode())))

	response, err := client.Do(req)
	if err != nil && response == nil {
		logrus.WithError(err).
			WithContext(context.Background()).
			Error("Error cccured trying to get user data from the users management API profile endpoint.")
		return nil, err
	}

	defer response.Body.Close()
	responseBody, err := ioutil.ReadAll(response.Body)

	if response.StatusCode >= 200 && response.StatusCode < 300 {
		var siginResponse apimodel.SigninResponse
		err := json.Unmarshal(responseBody, &siginResponse)
		if err != nil {
			logrus.WithError(err).
				WithContext(context.Background()).
				Error("Error parsing get profile successfully response from the users management API.")
			return nil, err
		}
		return &siginResponse, nil

	} else if response.StatusCode == http.StatusBadRequest {
		return nil, handleBadRequest(responseBody)
	} else if response.StatusCode == http.StatusUnauthorized || response.StatusCode == http.StatusForbidden {
		return nil, handleUnauthorizedResponse(responseBody)
	} else {
		logrus.WithField("ResponseStatusCode", response.StatusCode).
			Error("Error returned from the users management API profile endpoint")
		return nil, errors.New("Error returned from the users management API profile endpoint")
	}
}

// EditProfile - calls the edit progile endpoint from management users API
func (api *UsersManagementAPI) EditProfile(requestModel apimodel.EditProfileRequest, token auth.APIToken) error {
	body, err := json.Marshal(requestModel)
	if err != nil {
		logrus.WithError(err).
			WithContext(context.Background()).
			Error("Error occurred trying to serialize EditProfileRequest struct to json.")
		return err
	}

	endpoint := api.buildURL("api/profile")
	req, err := http.NewRequest("PUT", endpoint, bytes.NewBuffer([]byte(body)))
	if err != nil {
		logrus.WithError(err).
			WithContext(context.Background()).
			Error("Error cccured trying to create the users management API edit profile request.")
		return err
	}

	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Authorization", fmt.Sprintf("%s %s", token.GetTokenType(), token.Token))

	response, err := client.Do(req)
	if err != nil && response == nil {
		logrus.WithError(err).
			WithContext(context.Background()).
			Error("Error sending edit profile request to the users management API.")
		return err
	}
	defer response.Body.Close()
	responseBody, err := ioutil.ReadAll(response.Body)

	if response.StatusCode >= 200 && response.StatusCode < 300 {
		if err != nil {
			logrus.WithError(err).
				WithContext(context.Background()).
				Error("Error edit profile successfully response from the users management API.")
			return err
		}
		return nil
	} else if response.StatusCode == http.StatusBadRequest {
		return handleBadRequest(responseBody)
	} else if response.StatusCode == http.StatusUnauthorized {
		return ErrUnauthorized
	} else {
		logrus.WithField("ResponseStatusCode", response.StatusCode).
			Error("Error returned from the users management API signup request")
		return errors.New("Error returned from the users management API signup request")
	}
}

func (api *UsersManagementAPI) buildURL(path string) string {
	var sb strings.Builder
	sb.WriteString(api.APIUrl)
	if !strings.HasSuffix(api.APIUrl, "/") {
		sb.WriteString("/")
	}
	sb.WriteString(path)
	return sb.String()
}

type unauthorizedResponse struct {
	Error       string
	Description string
}

func handleUnauthorizedResponse(responseBody []byte) error {
	r := unauthorizedResponse{}
	err := json.Unmarshal(responseBody, &r)
	if err != nil {
		logrus.WithError(err).
			WithContext(context.Background()).
			Error("Error parsing unauthorized response from users management API request.")
		return err
	}

	if r.Error == "access_denied" {
		return ErrUnauthorizedClientApplication
	}
	return ErrInvalidGrant
}

func handleBadRequest(responseBody []byte) error {
	validationError := apimodel.APIValidationError{}
	err := json.Unmarshal(responseBody, &validationError)
	if err != nil {
		logrus.WithError(err).
			WithContext(context.Background()).
			Error("Error parsing bad request response from users management API signup request.")
		return err
	}
	return validationError
}
