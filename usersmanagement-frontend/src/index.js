import * as Toastr from 'toastr';
import './styles/main.scss';
import signup from './modules/signup'
import signin from './modules/signin'
import editProfile from './modules/editprofile'
import profile from './modules/profile'
import { recover, changePassword } from './modules/password'

Toastr.options = {
    "closeButton": true,
    "progressBar": false,
    "positionClass": "toast-top-center",
    "preventDuplicates": false,
    "newestOnTop": true,
    "timeOut": "5000",
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
  };

window.signup = signup;
window.signin = signin;
window.editProfile = editProfile;
window.profile = profile; 
window.recover = recover;
window.changePassword = changePassword;