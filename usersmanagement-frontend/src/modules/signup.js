import { post } from './methods'
import { urlEncodedForm } from './form'
import { handleErrors } from './handlers'

export default () => {
    const signupForm = document.getElementById('signupForm');
    if (signupForm) {
        signupForm.addEventListener("submit", (e) =>{
            e.preventDefault();

            var formData = urlEncodedForm(signupForm);
            
            post("/signup", formData)
                .then(res => handleErrors(res))
                .then(() => {
                    location.assign('/profile');
                })
                .catch(error => console.log(error));
        });
    }
}
