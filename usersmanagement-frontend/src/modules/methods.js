const baseUrl = process.env.APPLICATION_URL;

export const post = (url, body) => {
    const endpoint = `${baseUrl}${url}`;
    return fetch(endpoint, { method: 'POST', body: body, credentials: "include", mode: "cors" })
};


export const get = (url) => {
    const endpoint = `${baseUrl}${url}`;
    fetch(endpoint, { method: 'GET', credentials: "include" })
};


