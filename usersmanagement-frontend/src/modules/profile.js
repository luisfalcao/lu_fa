import { Map } from './map'
import { prepareLogout } from './session'

export default () => {
    prepareLogout();
    Map.loadGoogleMapsApi().then(function(googleMaps) {
        const addressElement = document.getElementById('address');
        const mapElement = document.getElementById('map');
        if (!mapElement || !addressElement) {
            return;
        }
        const map = Map.createMap(googleMaps, mapElement);
        const addressValue = addressElement.innerHTML;
        if (!addressValue) {
            return
        }
        const geocoder = new window.google.maps.Geocoder(); 

        geocoder.geocode( { 'address': addressValue}, function(results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                map.setCenter(results[0].geometry.location);

                new window.google.maps.Marker({
                    map: map,
                    position: results[0].geometry.location
                });
            }
        });  
    });
}
