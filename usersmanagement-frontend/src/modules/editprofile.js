import * as Toastr from 'toastr';
import { post } from './methods'
import { urlEncodedForm } from './form'
import { handleErrors } from './handlers'
import { Map } from './map'
import { prepareLogout } from './session'

export default () => {
    prepareLogout();
    const editProfileForm = document.getElementById('editProfileForm');
    
    Map.loadGoogleMapsApi({ libraries: ['places'] }).then(function(googleMaps) {
        if (editProfileForm) {
            editProfileForm.addEventListener("submit", (e) => {
                e.preventDefault();

                var formData = urlEncodedForm(editProfileForm);
                
                post("/edit", formData)
                    .then(res => handleErrors(res))
                    .then(() => {
                        Toastr.success("User profile updated");
                    })
                    .catch(error => console.log(error));
            });
        }
        
        const mapElement = document.getElementById('map-edit-form');
        if (!mapElement) {
            console.log("map not found");
            return;
        }
        const map = Map.createMap(googleMaps, mapElement);
        
        const addressElement = document.getElementById('address');

        const autocomplete = new google.maps.places.Autocomplete(addressElement);

        autocomplete.addListener('place_changed', handlePlaceChanged);

        const addressValue = addressElement.value;
        const geocoder = new window.google.maps.Geocoder();
        let marker;
        geocoder.geocode( { 'address': addressValue}, function(results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                if(marker) {
                    marker.setMap(null);
                }

                map.setCenter(results[0].geometry.location);
                marker = new window.google.maps.Marker({
                    map: map,
                    position: results[0].geometry.location
                });
            }
        });
        
        function handlePlaceChanged() {
            const place = autocomplete.getPlace();
            if(marker) {
                marker.setMap(null);
            }        
            map.setCenter(place.geometry.location);
            new window.google.maps.Marker({
                map: map,
                position: place.geometry.location
            });
        }
    }); 
}
