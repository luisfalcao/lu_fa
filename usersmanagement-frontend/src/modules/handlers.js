import * as Toastr from 'toastr';

export const handleErrors = (res) => {
    if (res.ok) {
        return res;
    }
    if (res.status == 400) {
        res.json().then(handleBadRequest);
    }
    if (res.status == 401) {
        location.assign('/index');
    }
    if (res.status >= 500) {
        handleServerError()
    }

    throw Error(res.statusText);
}

const handleBadRequest = (errorDetails) => {
    console.log(errorDetails);
    if (errorDetails.validations) {
        errorDetails.validations.forEach((validation) => {
            Toastr.warning(validation.error, validation.field);
        });
        return;
    }

    if (errorDetails.error) {
        Toastr.warning(errorDetails.error, 'Invalid data');       
    }
    else
        Toastr.error('Some of the provided information are not in a correct form.', 'Invalid data');
}

const handleServerError = (errorDetails) => {
    Toastr.error('An internal error occurred.', 'Internal error');
}