import * as Toastr from 'toastr';
import { post } from './methods';
import { urlEncodedForm } from './form';
import { handleErrors } from './handlers'

export const recover = () => {
    const recoverForm = document.getElementById('recoverForm');
    if (recoverForm) {
        recoverForm.addEventListener("submit", (e) =>{
            e.preventDefault();

            var formData = urlEncodedForm(recoverForm);
            
            post("/password/recover", formData)
                .then(res => handleErrors(res))
                .then(() => {
                    Toastr.success("An email will be sent to recover your password")
                })
                .catch(error => console.log(error));
        });
    }
}

export const changePassword = () => {
    const changePassword = document.getElementById('changePassword');
    if (changePassword) {
        changePassword.addEventListener("submit", (e) =>{
            e.preventDefault();

            var formData = urlEncodedForm(changePassword);
            
            post("/password/change", formData)
                .then(res => handleErrors(res))
                .then(() => {
                    Toastr.success("Your new password was saved.")
                })
                .catch(error => console.log(error));
        });
    }
}