export const urlEncodedForm = (form) => {
    var elements = form.querySelectorAll('input, textarea, select');
    var params = new URLSearchParams();
    for(var i = 0 ; i < elements.length ; i++){
        var item = elements.item(i);
        params.append(item.name, item.value);
    }
    return params;
}