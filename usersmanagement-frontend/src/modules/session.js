import { post, get } from './methods'

export const prepareLogout = () => {
    const logoutBtn = document.getElementById('logout-button');

    logoutBtn.addEventListener('click', function (e) {
        e.preventDefault();

        post('/logout')
            .then(() => window.location.replace('/'));
    });
}