import { post } from './methods'
import { urlEncodedForm } from './form'
import { handleErrors } from './handlers'

export default () => {
    const siginForm = document.getElementById('signinForm');
    if (siginForm) {
        siginForm.addEventListener("submit", (e) =>{
            e.preventDefault();

            var formData = urlEncodedForm(siginForm);
            
            post("/signin", formData)
                .then(res => handleErrors(res))
                .then(() => {
                    location.assign('/profile');
                })
                .catch(error => console.log(error));
        });
    }
}
