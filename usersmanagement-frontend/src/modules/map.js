import loadGoogleMapsApi from 'load-google-maps-api';

class Map {
  
  static loadGoogleMapsApi(options) {
    return loadGoogleMapsApi({ 
        key: process.env.GOOGLE_MAPS_APIKEY, // try to get that form a env file
        ...options,
    }); 
  }
  
  static createMap(googleMaps, mapElement) {
    return new googleMaps.Map(mapElement, {
      center: { lat: 45.520562, lng: -122.677438 },
      zoom: 14
    });
  }
}
export { Map };