package handlers

import "net/http"

// HandlePage - type for function that will execute the templates
type HandlePage func(http.ResponseWriter, string, interface{})
