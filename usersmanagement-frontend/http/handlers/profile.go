package handlers

import (
	ctx "context"
	"errors"
	"net/http"

	"bitbucket.org/luisfalcao/lu_fa/usersmanagement-frontend/apiclient"
	"bitbucket.org/luisfalcao/lu_fa/usersmanagement-frontend/apiclient/apimodel"
	"bitbucket.org/luisfalcao/lu_fa/usersmanagement-frontend/http/auth"
	"bitbucket.org/luisfalcao/lu_fa/usersmanagement-frontend/validations"
	"github.com/go-playground/validator/v10"
	schema "github.com/gorilla/Schema"
	"github.com/gorilla/context"
	"github.com/sirupsen/logrus"
)

// GetProfile - renders the profile user data
func GetProfile(api *apiclient.UsersManagementAPI, handlerPage HandlePage) http.HandlerFunc {
	return handleProfilePage("profile", api, handlerPage)
}

// EditProble - renders the profile user data
func EditProble(api *apiclient.UsersManagementAPI, handlerPage HandlePage) http.HandlerFunc {
	return handleProfilePage("edit", api, handlerPage)
}

type editProfileModel struct {
	Email     string `schema:"email" validate:"required,email"`
	Name      string `schema:"name" validate:"required"`
	Telephone string `schema:"telephone"`
	Address   string `schema:"address"`
}

// EditProfilePost - edit profile post handler
func EditProfilePost(validade *validator.Validate,
	decoder *schema.Decoder,
	api *apiclient.UsersManagementAPI) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		r.ParseForm()

		auth, err := getUserData(r)
		if err != nil {
			logrus.WithError(err).
				WithContext(ctx.Background()).
				Error("Error trying to get auth data from the context")
			UnauthorizedResponse(w)
		}

		m := editProfileModel{}

		if err := decoder.Decode(&m, r.PostForm); err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}

		err = validade.Struct(m)
		if err != nil {
			switch v := err.(type) {
			case validator.ValidationErrors:
				validationError := validations.NewValidationError(v)
				validationError.Error = "The content of some fields are invalid"
				JSONResponse(w, validationError, http.StatusBadRequest)
			default:
				http.Error(w, err.Error(), http.StatusInternalServerError)

			}
			return
		}

		request := apimodel.EditProfileRequest{
			Name:      m.Name,
			Email:     m.Email,
			Address:   m.Address,
			Telephone: m.Telephone,
		}

		err = api.EditProfile(request, auth)
		if err != nil {
			apiVlaidation := apimodel.APIValidationError{}
			switch {
			case errors.As(err, &apiVlaidation):
				JSONResponse(w, apiVlaidation, http.StatusBadRequest)
			case err == apiclient.ErrUnauthorized:
				logrus.WithError(err).
					WithContext(ctx.Background()).
					Error("The api returned unauthorized response")
				UnauthorizedResponse(w)
			default:
				logrus.WithError(err).
					WithContext(ctx.Background()).
					Error("Some error occurred in the api call")
				ErrorResponse(w)
			}
			return
		}

		w.WriteHeader(http.StatusOK)
	}
}

func handleProfilePage(page string, api *apiclient.UsersManagementAPI, handlerPage HandlePage) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {

		apiToken, err := getUserData(r)
		if err != nil {
			logrus.WithError(err).
				WithContext(ctx.Background()).
				Error("Error trying to get auth data from the context")
			http.Redirect(w, r, "/login", http.StatusFound)
		}

		user, err := api.GetUser(apiToken)
		if err != nil {
			switch err {
			case apiclient.ErrUnauthorized:
				logrus.WithError(err).
					WithContext(ctx.Background()).
					Error("Api returned unauthorized response")
				http.Redirect(w, r, "/login", http.StatusFound)
			default:
				logrus.WithError(err).
					WithContext(ctx.Background()).
					Error("Some error occurred in the api call")
				http.Redirect(w, r, "/login", http.StatusFound)
			}
			return
		}
		handlerPage(w, page, user)
	}
}

func getUserData(r *http.Request) (auth.APIToken, error) {
	if value := context.Get(r, "auth"); value != nil {
		return value.(auth.APIToken), nil
	}
	return auth.APIToken{}, errors.New("Auth data not in the context")
}
