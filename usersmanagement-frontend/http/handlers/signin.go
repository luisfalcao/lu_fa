package handlers

import (
	"context"
	"errors"
	"net/http"
	"time"

	"bitbucket.org/luisfalcao/lu_fa/usersmanagement-frontend/apiclient"
	"bitbucket.org/luisfalcao/lu_fa/usersmanagement-frontend/apiclient/apimodel"
	"bitbucket.org/luisfalcao/lu_fa/usersmanagement-frontend/http/auth"
	"bitbucket.org/luisfalcao/lu_fa/usersmanagement-frontend/validations"
	"github.com/go-playground/validator/v10"
	schema "github.com/gorilla/Schema"
	"github.com/gorilla/securecookie"
	"github.com/sirupsen/logrus"
)

type signinModel struct {
	Email    string `json:"email" validate:"required,email"`
	Password string `json:"password" validate:"required"`
}

// PostSign - post sign
func PostSign(validade *validator.Validate,
	decoder *schema.Decoder,
	s *securecookie.SecureCookie,
	api *apiclient.UsersManagementAPI) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		r.ParseForm()

		m := signinModel{}

		if err := decoder.Decode(&m, r.PostForm); err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}

		err := validade.Struct(m)
		if err != nil {
			switch v := err.(type) {
			case validator.ValidationErrors:
				validationError := validations.NewValidationError(v)
				validationError.Error = "The content of some fields are invalid"
				JSONResponse(w, validationError, http.StatusBadRequest)
			default:
				http.Error(w, err.Error(), http.StatusInternalServerError)

			}
			return
		}

		signinResponse, err := api.SigninUser(m.Email, m.Password)
		if err != nil {
			apiVlaidation := apimodel.APIValidationError{}
			switch { //verify the errors from the api, like 403 and 401
			case errors.As(err, &apiVlaidation):
				JSONResponse(w, apiVlaidation, http.StatusBadRequest)
			case err == apiclient.ErrInvalidGrant:
				logrus.WithError(err).
					WithContext(context.Background()).
					Errorf("Login attempt failed for user: %s", m.Email)
				UnauthorizedResponse(w)
			case err == apiclient.ErrUnauthorizedClientApplication:
				logrus.WithError(err).
					WithContext(context.Background()).
					Errorf("Application not authorized to generate tokens against the API")
				ErrorResponse(w)
			default:
				logrus.WithError(err).
					WithContext(context.Background()).
					Error("An error occurred trying to parse the SignupModel.")
				ErrorResponse(w)
			}
			return
		}

		authCookie := auth.APIToken{
			Token:        signinResponse.AccessToken,
			IsGoogleAuth: false,
		}

		user, err := api.GetUser(authCookie)
		if err != nil {
			switch err {
			case apiclient.ErrUnauthorized:
				logrus.WithError(err).
					WithContext(context.Background()).
					Error("The api returned unauthorized response")
				http.Error(w, "Unauthorized", http.StatusUnauthorized)
			default:
				logrus.WithError(err).
					WithContext(context.Background()).
					Error("Some error occurred in the api call")
				ErrorResponse(w)
			}
			return
		}
		authCookie.UserID = user.ID

		err = setAuthCookie(w, s, authCookie)
		if err != nil {
			logrus.WithError(err).
				WithContext(context.Background()).
				Error("Error occurred set the authentication cookie")
			ErrorResponse(w)
		}
		w.WriteHeader(http.StatusOK)
	}
}

func invalidateAuthCookie(w http.ResponseWriter) {
	cookie := http.Cookie{
		Name:    "Auth_Cookie",
		Value:   "",
		Path:    "/",
		Expires: time.Now().Add(0),
	}
	http.SetCookie(w, &cookie)
}

func setAuthCookie(w http.ResponseWriter,
	s *securecookie.SecureCookie,
	token auth.APIToken) error {

	encoded, err := s.Encode("Auth_Cookie", token)
	if err != nil {
		return err
	}

	cookie := http.Cookie{
		Name:    "Auth_Cookie",
		Value:   encoded,
		Path:    "/",
		Expires: time.Now().Add(time.Minute * 10),
	}
	http.SetCookie(w, &cookie)
	return nil
}
