package handlers

import (
	"encoding/json"
	"net/http"
)

// JSONResponse - write the struct provided as json in the response
func JSONResponse(w http.ResponseWriter, responseBody interface{}, code int) {
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	w.Header().Set("X-Content-Type-Options", "nosniff")
	w.WriteHeader(code)
	_ = json.NewEncoder(w).Encode(responseBody)
}

// ErrorResponseWithBody - write the struct provided as json in the response and set the http status code as internal server error
func ErrorResponseWithBody(w http.ResponseWriter, responseBody interface{}) {
	JSONResponse(w, responseBody, http.StatusInternalServerError)
}

// ErrorResponse - return a response with http status code as internal server error
func ErrorResponse(w http.ResponseWriter) {
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	w.Header().Set("X-Content-Type-Options", "nosniff")
	w.WriteHeader(http.StatusInternalServerError)
}

// UnauthorizedResponse - sets the response status code as unauthorized
func UnauthorizedResponse(w http.ResponseWriter) {
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	w.Header().Set("X-Content-Type-Options", "nosniff")
	w.WriteHeader(http.StatusUnauthorized)
}
