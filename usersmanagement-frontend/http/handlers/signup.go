package handlers

import (
	"context"
	"net/http"

	"bitbucket.org/luisfalcao/lu_fa/usersmanagement-frontend/apiclient"
	"bitbucket.org/luisfalcao/lu_fa/usersmanagement-frontend/apiclient/apimodel"
	"bitbucket.org/luisfalcao/lu_fa/usersmanagement-frontend/http/auth"
	"bitbucket.org/luisfalcao/lu_fa/usersmanagement-frontend/validations"
	"github.com/go-playground/validator/v10"
	schema "github.com/gorilla/Schema"
	"github.com/gorilla/securecookie"
	"github.com/sirupsen/logrus"
)

// SignupModel - model used to create news users
type SignupModel struct {
	Email           string `schema:"email" validate:"required,email"`
	Password        string `schema:"password" validate:"required,min=8"`
	ConfirmPassword string `schema:"confirmPassword" validate:"required,eqfield=Password"`
}

//PostSignup - handle the Signup page post request
func PostSignup(validade *validator.Validate,
	decoder *schema.Decoder,
	s *securecookie.SecureCookie,
	api *apiclient.UsersManagementAPI) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		r.ParseForm()

		var signupModel SignupModel
		err := decoder.Decode(&signupModel, r.PostForm)
		if err != nil {
			logrus.WithError(err).
				WithContext(context.Background()).
				Error("Error occurred trying to parse the SignupModel.")
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		err = validade.Struct(signupModel)
		if err != nil {
			switch v := err.(type) {
			case validator.ValidationErrors:
				validationError := validations.NewValidationError(v)
				validationError.Error = "The content of some fields are invalid"
				JSONResponse(w, validationError, http.StatusBadRequest)
			default:
				http.Error(w, err.Error(), http.StatusInternalServerError)
			}
			return
		}
		signupRequest := apimodel.SignupRequestRequest{
			Email:           signupModel.Email,
			Password:        signupModel.Password,
			ConfirmPassword: signupModel.ConfirmPassword,
		}

		tokenResponse, err := api.SignupUser(signupRequest)
		if err != nil {
			switch err.(type) {
			case apimodel.APIValidationError:
				apiError := err.(apimodel.APIValidationError)
				JSONResponse(w, apiError, http.StatusBadRequest)
			default:
				logrus.WithError(err).
					WithContext(context.Background()).
					Error("Error occurred trying to parse the SignupModel.")
				ErrorResponse(w)
			}
			return
		}

		authCookie := auth.APIToken{
			UserID:       tokenResponse.UserID,
			Token:        tokenResponse.AccessToken,
			IsGoogleAuth: false,
		}

		setAuthCookie(w, s, authCookie)

		w.WriteHeader(http.StatusCreated)
	}
}
