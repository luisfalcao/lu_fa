package handlers

import (
	"context"
	"errors"
	"net/http"

	"bitbucket.org/luisfalcao/lu_fa/usersmanagement-frontend/apiclient"
	"bitbucket.org/luisfalcao/lu_fa/usersmanagement-frontend/apiclient/apimodel"
	"bitbucket.org/luisfalcao/lu_fa/usersmanagement-frontend/validations"
	"github.com/go-playground/validator/v10"
	schema "github.com/gorilla/Schema"
	"github.com/sirupsen/logrus"
)

type recoverPasswordModel struct {
	Email string `schema:"email" validate:"required,email"`
}

// RecoverPasswordPost - recover password post handler
func RecoverPasswordPost(validade *validator.Validate,
	decoder *schema.Decoder,
	api *apiclient.UsersManagementAPI,
	recoverURL string,
) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		r.ParseForm()
		model := recoverPasswordModel{}

		if err := decoder.Decode(&model, r.PostForm); err != nil {
			logrus.WithError(err).
				WithContext(context.Background()).
				Error("An error occurred trying to decode the body")
			ErrorResponse(w)
		}

		err := validade.Struct(model)
		if err != nil {
			switch v := err.(type) {
			case validator.ValidationErrors:
				validationError := validations.NewValidationError(v)
				validationError.Error = "The content of some fields are invalid"
				JSONResponse(w, validationError, http.StatusBadRequest)
			default:
				http.Error(w, err.Error(), http.StatusInternalServerError)
			}
			return
		}

		request := apimodel.RecoverPasswordRequest{Email: model.Email, RecoverURL: recoverURL}

		if err := api.RecoverPassword(request); err != nil {
			apiVlaidation := apimodel.APIValidationError{}
			switch {
			case errors.As(err, &apiVlaidation):
				JSONResponse(w, apiVlaidation, http.StatusBadRequest)
			default:
				logrus.WithError(err).
					WithContext(context.Background()).
					Error("An error occurred in the api call")
				ErrorResponse(w)
			}
			return
		}

		w.WriteHeader(http.StatusOK)
	}
}

type changePasswordModel struct {
	Password        string `schema:"password" validate:"required,min=8"`
	ConfirmPassword string `schema:"confirmPassword" validate:"required"`
	Token           string `schema:"token" validate:"required"`
}

// ChangePasswordPost - recover password post handler
func ChangePasswordPost(validade *validator.Validate,
	decoder *schema.Decoder,
	api *apiclient.UsersManagementAPI,
) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		r.ParseForm()
		model := changePasswordModel{}

		if err := decoder.Decode(&model, r.PostForm); err != nil {
			logrus.WithError(err).
				WithContext(context.Background()).
				Error("An error occurred trying to decode the body")
			ErrorResponse(w)
			return
		}

		err := validade.Struct(model)
		if err != nil {
			switch v := err.(type) {
			case validator.ValidationErrors:
				validationError := validations.NewValidationError(v)
				validationError.Error = "The content of some fields are invalid"
				JSONResponse(w, validationError, http.StatusBadRequest)
			default:
				http.Error(w, err.Error(), http.StatusInternalServerError)
			}
			return
		}

		request := apimodel.ChangePasswordRequest{
			Password:        model.Password,
			ConfirmPassword: model.ConfirmPassword,
			Token:           model.Token,
		}

		if err := api.ChangePassword(request); err != nil {
			apiVlaidation := apimodel.APIValidationError{}
			switch {
			case errors.As(err, &apiVlaidation):
				JSONResponse(w, apiVlaidation, http.StatusBadRequest)
			default:
				logrus.WithError(err).
					WithContext(context.Background()).
					Error("An error occurred in the api call")
				ErrorResponse(w)
			}
			return
		}

		w.WriteHeader(http.StatusOK)
	}
}
