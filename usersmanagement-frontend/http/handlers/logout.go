package handlers

import "net/http"

// LogoutPost - logout from the applocation
func LogoutPost() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		invalidateAuthCookie(w)
		w.WriteHeader(http.StatusAccepted)
	}
}
