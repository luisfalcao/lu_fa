package handlers

import (
	"context"
	"net/http"

	"bitbucket.org/luisfalcao/lu_fa/usersmanagement-frontend/apiclient"
	"bitbucket.org/luisfalcao/lu_fa/usersmanagement-frontend/http/auth"
	"github.com/gorilla/securecookie"
	"github.com/sirupsen/logrus"
	"golang.org/x/oauth2"
)

// SigninGoogleHandler - google authentication callback handler
func SigninGoogleHandler(api *apiclient.UsersManagementAPI,
	s *securecookie.SecureCookie,
	handlerPage HandlePage,
	conf *oauth2.Config) http.Handler {

	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		cookie, err := r.Cookie("state")
		if err != nil {
			logrus.WithError(err).
				WithContext(context.Background()).
				Error("state cookie not found or some error happend trying to retrieve")
			http.Redirect(w, r, "/error", http.StatusInternalServerError)
			return
		}
		state := r.URL.Query().Get("state")

		if cookie.Value != state {
			logrus.WithContext(context.Background()).Error("The state value from coockie is different from the value returned ")
			http.Redirect(w, r, "/unauthorized", http.StatusUnauthorized)
		}
		code := r.URL.Query().Get("code")
		tok, err := conf.Exchange(oauth2.NoContext, code)
		if err != nil {
			logrus.WithError(err).
				WithContext(context.Background()).
				Error("Google token exchange failed")
			http.Redirect(w, r, "/unauthorized", http.StatusInternalServerError)
			return
		}

		signupRes, err := api.SigninGoogleUser(tok.AccessToken)
		if err != nil {
			logrus.WithError(err).
				WithContext(context.Background()).
				Error("An error occurred in the signup request for google users against the management users api")
			http.Redirect(w, r, "/Unauthorized", http.StatusUnauthorized)
			return
		}

		authCookie := auth.APIToken{
			UserID:       signupRes.UserID,
			Token:        tok.AccessToken,
			IsGoogleAuth: true,
		}
		setAuthCookie(w, s, authCookie)

		http.Redirect(w, r, "/", http.StatusFound)
	})
}
