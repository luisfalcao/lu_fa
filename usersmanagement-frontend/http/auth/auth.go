package auth

// APIToken - struct to work as the token
type APIToken struct {
	UserID       int64
	Token        string
	IsGoogleAuth bool
}

// GetTokenType - return the token type
func (c APIToken) GetTokenType() string {
	if c.IsGoogleAuth {
		return "OAuth2"
	} else {
		return "bearer"
	}
}
