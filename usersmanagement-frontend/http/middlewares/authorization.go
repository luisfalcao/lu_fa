package middlewares

import (
	ctx "context"
	"net/http"

	"bitbucket.org/luisfalcao/lu_fa/usersmanagement-frontend/apiclient"
	"bitbucket.org/luisfalcao/lu_fa/usersmanagement-frontend/http/auth"
	"bitbucket.org/luisfalcao/lu_fa/usersmanagement-frontend/http/handlers"
	"github.com/gorilla/context"
	"github.com/gorilla/securecookie"
	"github.com/sirupsen/logrus"
)

// RedirectAuthorizedUserMiddleware - handler that will redirect authenticated users that try to access signin or signup page
func RedirectAuthorizedUserMiddleware(s *securecookie.SecureCookie,
	api *apiclient.UsersManagementAPI,
	next http.Handler) http.Handler {

	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		cookie, _ := r.Cookie("Auth_Cookie")
		if cookie != nil {
			var authCookie auth.APIToken
			err := s.Decode("Auth_Cookie", cookie.Value, &authCookie)
			if err == nil {
				_, err = api.GetUser(authCookie)
				if err != nil {
					switch err {
					case apiclient.ErrUnauthorized:
						next.ServeHTTP(w, r)
					default:
						http.Redirect(w, r, "/error", http.StatusFound)
					}
					return
				}
				http.Redirect(w, r, "/profile", http.StatusFound)
			}
		}
		next.ServeHTTP(w, r)
	})
}

// AuthorizationMiddleware - protect endpoints that needs to be authenticated
func AuthorizationMiddleware(s *securecookie.SecureCookie,
	api *apiclient.UsersManagementAPI,
	next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		cookie, err := r.Cookie("Auth_Cookie")
		if err != nil {
			logrus.WithError(err).
				WithContext(ctx.Background()).
				Error("Authentication cookie not found or some error happend trying to retrieve")
			http.Redirect(w, r, "/", http.StatusFound)
			return
		}
		var authCookie auth.APIToken
		err = s.Decode("Auth_Cookie", cookie.Value, &authCookie)
		if err != nil {
			logrus.WithError(err).
				WithContext(ctx.Background()).
				Error("Error decoding the cookie.")
			http.Redirect(w, r, "/", http.StatusFound)
			return
		}

		_, err = api.GetUser(authCookie)

		if err != nil {
			switch err {
			case apiclient.ErrUnauthorized:
				http.Redirect(w, r, "/", http.StatusFound)
			default:
				http.Redirect(w, r, "/error", http.StatusFound)
			}
			return
		}

		context.Set(r, "auth", authCookie)
		next.ServeHTTP(w, r)
	})
}

// AjaxAuthorizationMiddleware - middleware to protect endpoints called with ajax
func AjaxAuthorizationMiddleware(s *securecookie.SecureCookie,
	api *apiclient.UsersManagementAPI,
	next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		cookie, err := r.Cookie("Auth_Cookie")
		if err != nil {
			logrus.WithError(err).
				WithContext(ctx.Background()).
				Error("Authentication cookie not found or some error happend trying to retrieve")
			handlers.UnauthorizedResponse(w)
			return
		}
		var authCookie auth.APIToken
		err = s.Decode("Auth_Cookie", cookie.Value, &authCookie)
		if err != nil {
			logrus.WithError(err).
				WithContext(ctx.Background()).
				Error("Error decoding the cookie.")
			handlers.UnauthorizedResponse(w)
			return
		}

		_, err = api.GetUser(authCookie)

		if err != nil {
			switch err {
			case apiclient.ErrUnauthorized:
				handlers.UnauthorizedResponse(w)
			default:
				handlers.UnauthorizedResponse(w)
			}
			return
		}

		context.Set(r, "auth", authCookie)
		next.ServeHTTP(w, r)
	})
}
