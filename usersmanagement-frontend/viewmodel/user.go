package viewmodel

// UserViewModel - user view model
type UserViewModel struct {
	Name, Email, Address, Telephone string
}
