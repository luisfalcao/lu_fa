module bitbucket.org/luisfalcao/lu_fa/usersmanagement-frontend

go 1.14

require (
	github.com/go-playground/locales v0.13.0
	github.com/go-playground/universal-translator v0.17.0
	github.com/go-playground/validator/v10 v10.3.0
	github.com/google/uuid v1.1.1
	github.com/gorilla/Schema v1.1.0
	github.com/gorilla/context v1.1.1
	github.com/gorilla/mux v1.7.4
	github.com/gorilla/securecookie v1.1.1
	github.com/nullseed/logruseq v0.0.0-20191022112445-275e5c09bb04
	github.com/sirupsen/logrus v1.6.0
	golang.org/x/net v0.0.0-20190620200207-3b0461eec859 // indirect
	golang.org/x/oauth2 v0.0.0-20200107190931-bf48bf16ab8d
	golang.org/x/sync v0.0.0-20190423024810-112230192c58 // indirect
	golang.org/x/sys v0.0.0-20200722175500-76b94024e4b6 // indirect
)
