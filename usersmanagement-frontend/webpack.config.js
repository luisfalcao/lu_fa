const path = require('path');
const HtmlWebPackPlugin = require("html-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const CopyPlugin = require('copy-webpack-plugin');
const Dotenv = require('dotenv-webpack');

module.exports = {
    mode: process.env.NODE_ENV || 'development',
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: [
                    {
                        loader: "babel-loader",
                    }
                ],
            },
            {
                test: /\.html$/,
                use: [
                    {
                        loader: "html-loader",
                        options: { minimize: true }
                    }
                ],
            },
            {
                test: /\.(png|svg|jpg|gif)$/,
                loader: 'file-loader',
                options: {
                    outputPath: 'resources/images',
                },
            },
            {
                test: /\.scss$/,
                loader: [
                    MiniCssExtractPlugin.loader,
                    'css-loader',
                    'sass-loader'
                ],
            }
        ]
    },
    plugins: [
        new HtmlWebPackPlugin({
            template: "./src/_layout.html",
            filename: "./_layout.html"
        }),
        new MiniCssExtractPlugin({
            filename: "resources/[name].css",
            chunkFilename: "resources/[id].css"
        }),
        new CopyPlugin({
            patterns: [
              { from: 'src/pages', to: './pages' },
            ],
        }),
        new Dotenv({
            path: './ui.env', 
        }),
    ],
    output: {
        filename: 'resources/main.js',
        path: path.resolve(__dirname, 'wwwroot')
    },
}