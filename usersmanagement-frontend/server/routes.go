package server

import (
	"context"
	"crypto/rand"
	"encoding/base64"
	"io/ioutil"
	"net/http"
	"os"
	"text/template"
	"time"

	"bitbucket.org/luisfalcao/lu_fa/usersmanagement-frontend/apiclient"
	"bitbucket.org/luisfalcao/lu_fa/usersmanagement-frontend/configurations"
	"bitbucket.org/luisfalcao/lu_fa/usersmanagement-frontend/http/handlers"
	"bitbucket.org/luisfalcao/lu_fa/usersmanagement-frontend/http/middlewares"
	"bitbucket.org/luisfalcao/lu_fa/usersmanagement-frontend/validations"
	"bitbucket.org/luisfalcao/lu_fa/usersmanagement-frontend/viewmodel"
	"golang.org/x/oauth2"
	"golang.org/x/oauth2/google"

	schema "github.com/gorilla/Schema"
	"github.com/gorilla/mux"
	"github.com/gorilla/securecookie"
	"github.com/sirupsen/logrus"
)

var decoder *schema.Decoder
var templates map[string]*template.Template
var validator = validations.CreateValidator()
var s = createSecureCookie()
var googleOAuthConfig = createGoogleOAuthConfig()
var usersmanagementapi = createUsersManagementAPI()
var recoverURL = configurations.GetRecovertURL()

func newRouter() http.Handler {
	decoder = schema.NewDecoder()
	templates = populateTemplate()
	router := mux.NewRouter()
	router.Use(middlewares.AddDefaultHeaders)

	// router.Use(middlewares.LogHandler) used only for testing proposes, it can make the logs too dirty

	router.Handle("/healthcheck", http.HandlerFunc(handlers.Healthcheck)).Methods("GET")

	//google auth callback
	router.Handle("/auth", handlers.SigninGoogleHandler(usersmanagementapi, s, handlePage, googleOAuthConfig))

	// POST Requests
	router.Handle("/signup", handlers.PostSignup(validator, decoder, s, usersmanagementapi)).Methods("POST")
	router.Handle("/signin", handlers.PostSign(validator, decoder, s, usersmanagementapi)).Methods("POST")
	router.Handle("/logout", handlers.LogoutPost()).Methods("POST")
	router.Handle("/password/recover", handlers.RecoverPasswordPost(validator, decoder, usersmanagementapi, recoverURL)).Methods("POST")
	router.Handle("/password/change", handlers.ChangePasswordPost(validator, decoder, usersmanagementapi)).Methods("POST")

	router.Handle("/edit",
		middlewares.AjaxAuthorizationMiddleware(s,
			usersmanagementapi,
			http.HandlerFunc(handlers.EditProfilePost(validator, decoder, usersmanagementapi)))).
		Methods("POST")

	// Authorization required UIs
	router.Handle("/profile",
		middlewares.AuthorizationMiddleware(s,
			usersmanagementapi,
			handlers.GetProfile(usersmanagementapi, handlePage))).
		Methods("GET")

	router.Handle("/edit",
		middlewares.AuthorizationMiddleware(s,
			usersmanagementapi,
			handlers.EditProble(usersmanagementapi, handlePage))).
		Methods("GET")

	// Authorization free UI
	router.Handle("/signup", middlewares.RedirectAuthorizedUserMiddleware(s,
		usersmanagementapi,
		http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			handlePage(w, "signup", nil)
		}))).Methods("GET")

	router.Handle("/recover", middlewares.RedirectAuthorizedUserMiddleware(s,
		usersmanagementapi,
		http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			handlePage(w, "recover", nil)
		}))).Methods("GET")

	router.Handle("/changepassword", middlewares.RedirectAuthorizedUserMiddleware(s,
		usersmanagementapi,
		http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			vm := &viewmodel.ChangePasswordViewModel{Token: r.URL.Query().Get("token")}
			handlePage(w, "changepassword", vm)
		}))).Methods("GET")

	indexHandler := middlewares.RedirectAuthorizedUserMiddleware(s,
		usersmanagementapi,
		http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			state := randToken()
			gURL := googleOAuthConfig.AuthCodeURL(state)
			vm := viewmodel.IndexViewModel{GoogleLoginURL: gURL}
			cookie := http.Cookie{
				Name:    "state",
				Value:   state,
				Path:    "/",
				Expires: time.Now().Add(time.Hour * 10),
			}
			http.SetCookie(w, &cookie)

			handlePage(w, "index", vm)
		}))
	router.Handle("/", indexHandler).Methods("GET")
	router.Handle("/index", indexHandler).Methods("GET")

	router.Handle("/{page}", http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		v := mux.Vars(r)
		handlePage(w, v["page"], nil)
	}))

	// resources
	fs := http.FileServer(http.Dir("./wwwroot"))
	router.PathPrefix("/resources/").Handler(fs)

	http.Handle("/", router)

	return router
}

func randToken() string {
	b := make([]byte, 32)
	rand.Read(b)
	return base64.StdEncoding.EncodeToString(b)
}

func createSecureCookie() *securecookie.SecureCookie {
	var (
		hashkey  = configurations.GetSecureCookieHashKey()
		blockKey = configurations.GetSecureCookieBlockKey()
	)
	return securecookie.New([]byte(hashkey), []byte(blockKey))
}

func createUsersManagementAPI() *apiclient.UsersManagementAPI {
	var (
		url          = configurations.GetUsersManagementAPIURL()
		clientID     = configurations.GetUsersManagementClientID()
		clientSecret = configurations.GetUsersManagementClientSecret()
	)
	return apiclient.NewUsersManagementAPI(url, clientID, clientSecret)
}

func createGoogleOAuthConfig() *oauth2.Config {
	var conf = &oauth2.Config{
		ClientID:     configurations.GetGoogleClientID(),
		ClientSecret: configurations.GetGoogleClientSecret(),
		RedirectURL:  configurations.GetGoogleRedirectURL(),
		Scopes: []string{
			"https://www.googleapis.com/auth/userinfo.email",
			"https://www.googleapis.com/auth/userinfo.profile",
		},
		Endpoint: google.Endpoint,
	}
	return conf
}

func handlePage(w http.ResponseWriter, page string, data interface{}) {
	t := templates[page+".html"]

	if t != nil {
		err := t.Execute(w, data)
		if err != nil {
			logrus.WithError(err).
				WithContext(context.Background()).
				Error("Error trying to execute template")
		}
	} else {
		w.WriteHeader(http.StatusNotFound)
	}
}

func populateTemplate() map[string]*template.Template {
	result := make(map[string]*template.Template)
	const basePath = "wwwroot"

	layout := template.Must(template.ParseFiles(basePath + "/_layout.html"))

	dir, err := os.Open(basePath + "/pages")
	if err != nil {
		panic("Failed to open template blocks directory " + err.Error())
	}

	fis, err := dir.Readdir(-1)
	if err != nil {
		panic("Failed to read contents of the content directory " + err.Error())
	}

	for _, fi := range fis {
		f, err := os.Open(basePath + "/pages/" + fi.Name())
		if err != nil {
			panic("Failed to open template '" + fi.Name() + "'")
		}
		content, err := ioutil.ReadAll(f)

		if err != nil {
			panic("Failed to read content from the file '" + fi.Name() + "'")
		}
		f.Close()

		tmpl := template.Must(layout.Clone())
		_, err = tmpl.Parse(string(content))
		if err != nil {
			panic("Failed to parse contents of '" + fi.Name() + "' as template")
		}
		result[fi.Name()] = tmpl
	}
	return result
}
