package server

import (
	"fmt"
	"net/http"
	"time"

	"github.com/sirupsen/logrus"
)

// StartServer function that configure and start to listening some port to receive http requests
func StartServer(wait chan bool, port string) *http.Server {

	addr := fmt.Sprintf(":%s", port)
	logrus.Infof("Staring http server on %s", addr)

	srv := &http.Server{
		Addr:         addr,
		Handler:      newRouter(),
		ReadTimeout:  time.Second * 5,
		WriteTimeout: time.Second * 10,
		IdleTimeout:  time.Second * 120,
	}

	go func() {
		logrus.Infof("Http server running on %s", addr)
		if err := srv.ListenAndServe(); err != nil {
			logrus.Errorf("error running the server: %v", err)
			close(wait)
		}
	}()

	return srv
}
