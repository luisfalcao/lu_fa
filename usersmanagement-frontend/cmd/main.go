package main

import (
	"context"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"bitbucket.org/luisfalcao/lu_fa/usersmanagement-frontend/apiclient"
	"bitbucket.org/luisfalcao/lu_fa/usersmanagement-frontend/configurations"
	"bitbucket.org/luisfalcao/lu_fa/usersmanagement-frontend/server"
	"github.com/nullseed/logruseq"
	"github.com/sirupsen/logrus"
)

func main() {
	if logURL := configurations.GetSeqURL(); len(logURL) != 0 {
		logrus.SetReportCaller(true)
		logrus.AddHook(logruseq.NewSeqHook(logURL))
	}

	logrus.Infof("Started Users Management application (pid:%v; uid:%v; gid:%v)", os.Getpid(), os.Getuid(), os.Getgid())

	client := configurations.ConfigureClient()
	defer client.CloseIdleConnections()

	sigChannel := make(chan os.Signal)
	signal.Notify(sigChannel, os.Interrupt, syscall.SIGTERM)
	serverDone := make(chan bool)
	srv := server.StartServer(serverDone, os.Getenv("PORT"))

	apiclient.SetHTTPClient(client)

	<-sigChannel
	go shutdownServer(srv)
	<-serverDone

	logrus.Info("Serve was stopped.")
}

func shutdownServer(server *http.Server) {
	ctxShutDown, cancelContext := context.WithTimeout(context.Background(), time.Second*6)
	defer cancelContext()

	err := server.Shutdown(ctxShutDown)
	if err != nil {
		logrus.WithError(err).Error("Error when shutting down the server (%s): %v", server.Addr, err)

		err = server.Close()
		if err != nil {
			logrus.WithError(err).Error("Error trying to close the server (%s): %v", server.Addr, err)
		}
	}
}
