package validations

import (
	"github.com/go-playground/locales/en"
	ut "github.com/go-playground/universal-translator"
	"github.com/go-playground/validator/v10"
	en_translations "github.com/go-playground/validator/v10/translations/en"
)

type translation struct {
	tag, message string
}

var translations = []translation{
	{tag: "required", message: "{0} is a required field"},
	{tag: "email", message: "{0} must be a valid email"},
}

var trans ut.Translator

// ValidationError - struct that will be returned in bad requests responses
type ValidationError struct {
	Error       string            `json:"error"`
	Validations []FieldValidation `json:"validations,omitempty"`
}

// FieldValidation - sctruct to represent model fields validations
type FieldValidation struct {
	Field string `json:"field"`
	Error string `json:"error"`
}

// CreateValidator - creates a instance of validator.Validate with validations message configured
func CreateValidator() *validator.Validate {
	validate := validator.New()

	en := en.New()
	uni := ut.New(en, en)
	trans, _ = uni.GetTranslator("en")

	en_translations.RegisterDefaultTranslations(validate, trans)

	for _, translation := range translations {
		_ = validate.RegisterTranslation(translation.tag,
			trans,
			registrationFunc(translation.tag, translation.message),
			translateFunc(translation.tag),
		)
	}

	return validate
}

func registrationFunc(tag string, translation string) validator.RegisterTranslationsFunc {
	return func(ut ut.Translator) (err error) {
		if err = ut.Add(tag, translation, true); err != nil {
			return
		}
		return
	}
}

func translateFunc(tag string) validator.TranslationFunc {
	return func(ut ut.Translator, fe validator.FieldError) string {
		t, _ := ut.T(tag, fe.Field())
		return t
	}
}

// NewValidationError - map struct validator.ValidationErrors to ValidationError
func NewValidationError(v validator.ValidationErrors) (validationError ValidationError) {
	validationError = ValidationError{Validations: make([]FieldValidation, 0)}

	for _, e := range v {
		fieldVal := FieldValidation{
			Field: e.Field(),
			Error: e.Translate(trans),
		}
		validationError.Validations = append(validationError.Validations, fieldVal)
	}

	return
}
