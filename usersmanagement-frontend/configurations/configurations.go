package configurations

import (
	"fmt"
	"net/http"
	"os"
	"time"
)

const (
	maxIdleConnections int = 20
	requestTimeout     int = 5
)

// GetSecureCookieHashKey - Gets the secure cookie hash key from the OS environment variables
func GetSecureCookieHashKey() string {
	return getosEnv("SECURITY_TOKEN_KEY")
}

// GetSecureCookieBlockKey -  Gets the token from the OS environment variables
func GetSecureCookieBlockKey() string {
	return getosEnv("SECURITY_TOKEN_BLOCK_KEY")
}

// GetUsersManagementAPIURL - Gets the users management URL from the OS environment variables
func GetUsersManagementAPIURL() string {
	return getosEnv("USERS_MANAGEMENT_API_URL")
}

// GetUsersManagementClientID - Gets the users management clientID from the OS environment variables
func GetUsersManagementClientID() string {
	return getosEnv("CLIENT_ID")
}

// GetUsersManagementClientSecret - Gets the users management clientSecret from the OS environment variables
func GetUsersManagementClientSecret() string {
	return getosEnv("CLIENT_SECRET")
}

// GetGoogleClientID - Gets the google clientID from the OS environment variables
func GetGoogleClientID() string {
	return getosEnv("GOOGLE_CLIENT_ID")
}

// GetGoogleClientSecret -  Gets the google clientID from the OS environment variables
func GetGoogleClientSecret() string {
	return getosEnv("GOOGLE_CLIENT_SECRET")
}

// GetGoogleRedirectURL -  Gets the google redirect url from the OS environment variables
func GetGoogleRedirectURL() string {
	return getosEnv("GOOGLE_REDIRECT_URL")
}

// GetRecovertURL -  Gets the recover url from the OS environment variables
func GetRecovertURL() string {
	return getosEnv("RECOVER_URL")
}

// GetSeqURL -  Gets the seq url from the OS environment variables
func GetSeqURL() string {
	return os.Getenv("SEQ_URL")
}

//GOOGLE_REDIRECT_URL
func getosEnv(variable string) string {
	value := os.Getenv(variable)
	if len(value) == 0 {
		panic(fmt.Sprintf("%s not present in the environment variables", variable))
	}
	return value
}

// ConfigureClient - configures the http client in the package
func ConfigureClient() *http.Client {
	return &http.Client{
		Transport: &http.Transport{
			MaxIdleConnsPerHost: maxIdleConnections,
		},
		Timeout: time.Duration(requestTimeout) * time.Second,
	}
}
