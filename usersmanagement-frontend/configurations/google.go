package configurations

// GoogleCredentials - stores google client id and client secret .
type GoogleCredentials struct {
	ClientID     string
	ClientSecret string
}