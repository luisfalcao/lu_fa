CREATE TABLE IF NOT EXISTS  `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) NOT NULL UNIQUE,
  `telephone` varchar(15) NULL,
  `password` varchar(255) DEFAULT NULL,
  `address` mediumtext  DEFAULT NULL,
  `google_signup` tinyint(1) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
);

CREATE TABLE IF NOT EXISTS `recover_password_tokens` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `token` varchar(255) NOT NULL,
  `expiration_time` timestamp NOT NULL,
  PRIMARY KEY(`id`) 
)