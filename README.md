# Users Management

Hello, this is the Users Management application repository.

This repository contains two projects, one for the frontend (in the folter usersmanagement-frontend) and another one for the API (in the filter usersmanagement-api).

You can run this project in two ways.

- You can run each project with the make file. If you navigate to the folder of one of the projects and run the command `make run` it will run the application inside that folder. But before that, you need to create a copy of the file `dev.env.sample` and rename to just dev.env in each project.
Also, it's needed to substitute some values inside this file. 
In your mysql server, you will need to create a database with the name setup in the variable `DB_DATABASE` in the env file and run the sql script in the path `mysql/setup.sql`. 

- It's a good idea to go with this option because the MySQL server will run and also set up the default database for you. And here as well, you will need to create docker.env files for each project, based on the `docker.env.sample` file.


For both steps, it's necessary to do the same process of copying and edit a file for the file `usersmanagement-frontend/ui.env.sample`.
Create a copy of this file without the `suffix .sample`. The variable `APPLICATION_URL` is the base URL of the frontend application itself. So if you are running locally, in the port 3000, put the value `http://localhost:3000` (without trailing slash at the end)


An example of this application is running at http://lu_fa.azurewebsites.net/.