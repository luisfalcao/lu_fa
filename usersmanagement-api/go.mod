module bitbucket.org/luisfalcao/lu_fa/usersmanagement-api

go 1.14

require (
	bitbucket.org/luisfalcao/lu_fa/usersmanagement-frontend v0.0.0-20200723161830-c30ed407f4c9
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/go-playground/locales v0.13.0
	github.com/go-playground/universal-translator v0.17.0
	github.com/go-playground/validator/v10 v10.3.0
	github.com/go-sql-driver/mysql v1.5.0
	github.com/golang/protobuf v1.4.2 // indirect
	github.com/google/uuid v1.1.1
	github.com/gorilla/Schema v1.1.0
	github.com/gorilla/context v1.1.1
	github.com/gorilla/mux v1.7.4
	github.com/rs/zerolog v1.19.0
	golang.org/x/crypto v0.0.0-20200709230013-948cd5f35899
	golang.org/x/net v0.0.0-20200707034311-ab3426394381 // indirect
	golang.org/x/oauth2 v0.0.0-20200107190931-bf48bf16ab8d // indirect
	google.golang.org/appengine v1.6.6 // indirect
	google.golang.org/protobuf v1.25.0 // indirect
)
