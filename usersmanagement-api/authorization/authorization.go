package authorization

import (
	"errors"
	"fmt"
	"net/http"
	"os"
)

// AuthorizedApplication - represents the configuration for an authorized application
type AuthorizedApplication struct {
	ClientID     string
	ClientSecret string
	GrandType    string
}

// CreateDefaultAuthorizedApplication - creates a default authorized application
func CreateDefaultAuthorizedApplication() (AuthorizedApplication, error) {
	var c AuthorizedApplication
	c.ClientID = os.Getenv("ALLOWED_CLIENT_ID")
	if len(c.ClientID) == 0 {
		return AuthorizedApplication{}, errors.New("ALLOWED_CLIENT_ID not found in the OS environment variables")
	}

	c.ClientSecret = os.Getenv("ALLOWED_CLIENT_SECRET")
	if len(c.ClientSecret) == 0 {
		return AuthorizedApplication{}, errors.New("ALLOWED_CLIENT_SECRET not found in the OS environment variables")
	}

	c.GrandType = "password"
	return c, nil
}

// Validate - validates the authorized application against the supplied information. Returns nil if it is valid. Returns AuthorizedApplicationInvalidError error if is invalid.
func (a AuthorizedApplication) Validate(clientID, clientSecret, grantType string) error {
	if a.ClientID != clientID || a.ClientSecret != clientSecret {
		return AuthorizedApplicationInvalidError{
			Type:           AccessDenied,
			Description:    "The authorized application credentials are invalid",
			HTTPStatusCode: http.StatusUnauthorized,
		}
	}
	if a.GrandType != grantType {
		return AuthorizedApplicationInvalidError{
			Type:           InvalidGrant,
			Description:    fmt.Sprintf("Grant type '%s' invalid or not allowed for the client.", grantType),
			HTTPStatusCode: http.StatusForbidden,
		}
	}
	return nil
}

// AuthorizedApplicationInvalidError - a struct to represent a error when the authorized application information is invalid
type AuthorizedApplicationInvalidError struct {
	Type           AuthorizedApplicationErrorType
	Description    string
	HTTPStatusCode int
}

// AuthorizedApplicationErrorType - type to represent the autorize application error types
type AuthorizedApplicationErrorType string

// Values for AuthorizedApplicationErrorType enum
const (
	AccessDenied AuthorizedApplicationErrorType = "access_denied"
	InvalidGrant                                = "invalid_grant"
)

func (a AuthorizedApplicationInvalidError) Error() string {
	return a.Description
}
