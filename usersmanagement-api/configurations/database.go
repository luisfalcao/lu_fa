package configurations

import (
	"database/sql"
	"os"
	"time"

	"bitbucket.org/luisfalcao/lu_fa/usersmanagement-api/model"
)

// ConfigureDatabase - Configure the access to the database
func ConfigureDatabase() (db *sql.DB, err error) {
	dbHost := os.Getenv("DB_HOST")
	dbPort := os.Getenv("DB_PORT")
	dbUser := os.Getenv("DB_USER")
	dbPassword := os.Getenv("DB_PASSWORD")
	databaseName := os.Getenv("DB_DATABASE")

	db, err = model.Connect(dbHost, dbPort, dbUser, dbPassword, databaseName)
	if err != nil {
		return nil, err
	}

	db.SetConnMaxLifetime(time.Second * 60)
	return
}
