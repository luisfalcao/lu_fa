package main

import (
	"context"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"bitbucket.org/luisfalcao/lu_fa/usersmanagement-api/configurations"
	"bitbucket.org/luisfalcao/lu_fa/usersmanagement-api/model"
	"bitbucket.org/luisfalcao/lu_fa/usersmanagement-api/server"
	"github.com/rs/zerolog/log"
)

func main() {
	log.Info().Msgf("Started application (pid:%v; uid:%v; gid:%v)", os.Getpid(), os.Getuid(), os.Getgid())

	sigChannel := make(chan os.Signal)
	signal.Notify(sigChannel, os.Interrupt, syscall.SIGTERM)
	serverDone := make(chan bool)

	db, err := configurations.ConfigureDatabase()
	defer db.Close()
	if err != nil {
		panic(err)
	}
	model.SetDatabase(db)

	srv := server.StartServer(serverDone, 8080)

	<-sigChannel
	go shutdownServer(srv)
	<-serverDone

	log.Info().Msg("Serve was stopped.")
}

func shutdownServer(server *http.Server) {
	ctxShutDown, cancelContext := context.WithTimeout(context.Background(), time.Second*6)
	defer cancelContext()

	err := server.Shutdown(ctxShutDown)
	if err != nil {
		log.Error().Msgf("Error when shutting down the server (%s): %v", server.Addr, err)

		err = server.Close()
		if err != nil {
			log.Error().Msgf("Error trying to close the server (%s): %v", server.Addr, err)
		}
	}
}
