package apimodel

import (
	"time"
)

// SignupModel - model for signup with email and password
type SignupModel struct {
	Email           string `json:"email" validate:"required,email"`
	Password        string `json:"password" validate:"required,min=8"`
	ConfirmPassword string `json:"confirmPassword" validate:"required,eqfield=Password"`
}

// SignupResultModel - model to return when the users successfully sign up
type SignupResultModel struct {
	UserID    int64   `json:"user_id"`
	Token     string  `json:"access_token"`
	TokenType string  `json:"token_Type"`
	ExpiresIn float64 `json:"expires_in"`
}

// ProfileResult - model to return the user info
type ProfileResult struct {
	ID          int64     `json:"user_id"`
	Name        string    `json:"name,omitempty"`
	Email       string    `json:"email,omitempty"`
	Telephone   string    `json:"telephone,omitempty"`
	Address     string    `json:"address,omitempty"`
	CreatedDate time.Time `json:"created_date"`
}

// ProfileUpdateModel - model used to update the user data in the database
type ProfileUpdateModel struct {
	Name      string `json:"name" validate:"required"`
	Email     string `json:"email" validate:"omitempty,email"`
	Telephone string `json:"telephone"`
	Address   string `json:"address"`
}

// TokenRequestValidationError - model used to return a token validation error
type TokenRequestValidationError struct {
	Error       string `json:"error"`
	Description string `json:"error_description"`
}

// AuthorizedTokenResult - model to return when the users successfully sign in
type AuthorizedTokenResult struct {
	Token     string  `json:"access_token"`
	TokenType string  `json:"token_Type"`
	ExpiresIn float64 `json:"expires_in"`
}

// RecoverPasswordRequestModel -model for send recover password email
type RecoverPasswordRequestModel struct {
	RecoverURL string `json:"recoverUrl" validate:"required"`
	Email      string `json:"email" validate:"required,email"`
}

// ChangePasswordModel - model with the data to change the password
type ChangePasswordModel struct {
	Token           string `json:"token"`
	Password        string `json:"password" validate:"required,min=8"`
	ConfirmPassword string `json:"confirmPassword" validate:"required,eqfield=Password"`
}
