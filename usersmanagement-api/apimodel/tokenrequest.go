package apimodel

// TokenRequest - struct with the data to generate a user token
type TokenRequest struct {
	ClientID     string `schema:"client_id" validate:"required"`
	ClientSecret string `schema:"client_secret" validate:"required"`
	GrandType    string `schema:"grant_type" validate:"required"`
	Username     string `schema:"username" validate:"required"`
	Password     string `schema:"password" validate:"required"`
}
