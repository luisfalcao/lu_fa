package mail

import (
	"bytes"
	"fmt"
	"mime/quotedprintable"
	"net/smtp"

	"github.com/rs/zerolog/log"
)

// Mail - mail service
type Mail struct {
	smtpServer, smtpUser, smtpPassword string
	smtpPort                           int
}

// Message - struct with the data that will be sent by email
type Message struct {
	Message string
	From    string
	To      string
	Subject string
}

// New - creates a new mail service
func New(smtpServer, smtpUser, smtpPassword string, smtpPort int) *Mail {
	return &Mail{
		smtpServer:   smtpServer,
		smtpUser:     smtpUser,
		smtpPassword: smtpPassword,
		smtpPort:     smtpPort,
	}
}

// Send - sends an email with the data supplied in the Message struct
func (m *Mail) Send(message Message) error {
	auth := smtp.PlainAuth("", m.smtpUser, m.smtpPassword, m.smtpServer)
	header := buildHeader(message)

	var bodyMessage bytes.Buffer
	temp := quotedprintable.NewWriter(&bodyMessage)
	temp.Write([]byte(message.Message))
	temp.Close()

	mailMessage := header + "\r\n" + bodyMessage.String()

	err := smtp.SendMail(m.getHost(), auth, message.From, []string{message.To}, []byte(mailMessage))
	if err != nil {
		log.Err(err).Msg("An error occured trying to send an email")
		return err
	}
	return nil
}

func (m *Mail) getHost() string {
	return fmt.Sprintf("%s:%d", m.smtpServer, m.smtpPort)
}

func buildHeader(message Message) string {
	header := make(map[string]string)

	header["From"] = message.From
	header["To"] = message.To
	header["Subject"] = message.Subject
	header["MIME-Version"] = "1.0"
	header["Content-Type"] = fmt.Sprintf("%s; charset=\"utf-8\"", "text/html")
	header["Content-Disposition"] = "inline"
	header["Content-Transfer-Encoding"] = "quoted-printable"

	headerString := ""
	for key, value := range header {
		headerString += fmt.Sprintf("%s: %s\r\n", key, value)
	}
	return headerString
}
