package passwords

import (
	"github.com/rs/zerolog/log"
	"golang.org/x/crypto/bcrypt"
)

const salt = 10

// Encrypt - encrypts the password provided
func Encrypt(password string) (string, error) {
	encrypted, err := bcrypt.GenerateFromPassword([]byte(password), salt)
	return string(encrypted), err
}

// Compare - compare the password with a hash value
func Compare(hash, pass string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(pass))
	if err != nil {
		log.Err(err).Caller().Msg("Error comparing passwords")
		return false
	}
	return true
}
