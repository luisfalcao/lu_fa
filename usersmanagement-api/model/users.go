package model

import (
	"database/sql"
	"errors"
	"fmt"
	"time"

	"bitbucket.org/luisfalcao/lu_fa/usersmanagement-api/model/passwords"
)

// UsersStorage - type to handle the crud operations for User type
type UsersStorage struct {
}

// User - a representation for the users database table
type User struct {
	ID int64
	Email,
	Name,
	Password,
	Telephone,
	Address string
	GoogleSignUp bool
	CreatedDate  time.Time
}

// RecoverPasswordToken - request password token
type RecoverPasswordToken struct {
	UserID     int64
	Expiration time.Time
}

// CreateUser - Creates the user in database. Returns the created user ID
func (us *UsersStorage) CreateUser(email, password string) (int64, error) {
	emailAvailable, err := us.emailAvailable(email)

	switch {
	case err != nil:
		return 0, err
	case !emailAvailable:
		return 0, ErrEmailNotAvailable
	}

	passwordHash, err := passwords.Encrypt(password)
	if err != nil {
		return 0, fmt.Errorf("An error occurred trying to encrypt the password. Error: %w", err)
	}

	result, err := Db.Exec(`
		INSERT INTO users (email, name, password, google_signup) 
		VALUES (?, ?, ?, ?)`, email, nil, passwordHash, false)

	if err != nil {
		return 0, err
	}

	return result.LastInsertId()
}

// CreateRecoverPasswordToken - saves in the database the token
func (us *UsersStorage) CreateRecoverPasswordToken(token string, userID int64, expiration time.Time) error {
	_, err := Db.Exec(`
		INSERT INTO recover_password_tokens (token, user_id, expiration_time) 
		VALUES (?, ?, ?)`, token, userID, expiration)

	if err != nil {
		return err
	}
	return nil
}

// DeleteRecoverPassword - delete recover password token
func (us *UsersStorage) DeleteRecoverPassword(token string) error {
	_, err := Db.Exec(`
		DELETE FROM recover_password_tokens
		WHERE token = ?`, token)

	return err
}

// GetRecoverPassword - return an change password token from the database
func (us *UsersStorage) GetRecoverPassword(token string) (RecoverPasswordToken, error) {
	row := Db.QueryRow(
		`select user_id, expiration_time 
		from recover_password_tokens 
		where token = ?`, token)

	r := RecoverPasswordToken{}
	err := row.Scan(&r.UserID, &r.Expiration)

	switch {
	case err == sql.ErrNoRows:
		return RecoverPasswordToken{}, ErrUserNotFound
	case err != nil:
		return RecoverPasswordToken{}, err
	}

	return r, nil
}

// GetUserByID - returns an user that matches the id
func (us *UsersStorage) GetUserByID(id int64) (*User, error) {
	row := Db.QueryRow(
		`select id, email, name, password, telephone, address, google_signup, created_date
		from users 
		where id = ?`, id)

	return mapUser(row)
}

// GetUserByEmail - returns an user that matches the email
func (us *UsersStorage) GetUserByEmail(email string) (*User, error) {
	row := Db.QueryRow(
		`select id, email, name, password, telephone, address, google_signup, created_date
		from users 
		where email = ?`, email)

	return mapUser(row)
}

// CreateUserFromGoogle - Creates the user in database. Returns the created user ID
func (us *UsersStorage) CreateUserFromGoogle(email, name string) (int64, error) {
	emailAvailable, err := us.emailAvailable(email)

	switch {
	case err != nil:
		return 0, err
	case !emailAvailable:
		return 0, ErrEmailNotAvailable
	}

	result, exeError := Db.Exec(`
		INSERT INTO users (email, name, google_signup) 
		VALUES (?, ?, ?)`, email, name, true)

	if exeError != nil {
		return 0, exeError
	}

	return result.LastInsertId()
}

// Update - update the user in the database. Returns the updated user
func (us *UsersStorage) Update(userID int64,
	email, name, telephone, address string) (*User, error) {

	user, err := us.FindUserByID(userID)
	if err != nil {
		return nil, err
	}
	err = updateUserStruct(user, email, name, telephone, address)
	if err != nil {
		return nil, err
	}

	_, err = Db.Exec(`
		UPDATE users
		set name = ?, email = ?, telephone = ?, address = ?
		where id = ?`,
		user.Name, user.Email, user.Telephone, user.Address, user.ID)

	if err != nil {
		return nil, err
	}
	return user, nil
}

// UpdatePassword - updates the user password
func (us *UsersStorage) UpdatePassword(newPassword string, userID int64) error {
	passwordHash, err := passwords.Encrypt(newPassword)
	if err != nil {
		return fmt.Errorf("An error occurred trying to encrypt the password. Error: %w", err)
	}

	_, err = Db.Exec(`
		UPDATE users
		set password = ?
		where id = ?`, passwordHash, userID)

	return err
}

func updateUserStruct(user *User, email, name, telephone, address string) error {
	if len(email) != 0 && user.Email != email {
		available, err := emailAvailableForUser(email, user)
		if user.GoogleSignUp {
			return ErrGoogleAccountCanChangeTheEmail
		}
		if err != nil {
			return err
		}
		if !available {
			return ErrEmailNotAvailable
		}
		user.Email = email
	}
	user.Name = name
	user.Telephone = telephone
	user.Address = address
	return nil
}

// FindUserByEmailAndPassword - Search in the database for a user with the provided email and password
func (us *UsersStorage) FindUserByEmailAndPassword(email, password string) (*User, error) {

	row := Db.QueryRow(
		`select id, email, name, password, telephone, address, google_signup, created_date
		from users 
		where email = ?`, email)

	user, err := mapUser(row)
	if err != nil {
		return nil, err
	}
	if !passwords.Compare(user.Password, password) {
		return nil, ErrWrongPassword
	}
	return user, nil
}

// FindUserByID - retrieves an user that matches the id provided.
func (us *UsersStorage) FindUserByID(userID int64) (*User, error) {
	row := Db.QueryRow(
		`select id, email, name, password, telephone, address, google_signup, created_date
		from users 
		where id = ?`, userID)

	return mapUser(row)
}

func mapUser(row Row) (*User, error) {
	user := &User{}
	var name, address, password, telephone sql.NullString

	err := row.Scan(&user.ID, &user.Email,
		&name, &password,
		&telephone, &address,
		&user.GoogleSignUp, &user.CreatedDate)

	switch {
	case err == sql.ErrNoRows:
		return nil, ErrUserNotFound
	case err != nil:
		return nil, err
	}

	if name.Valid {
		user.Name = name.String
	}

	if address.Valid {
		user.Address = address.String
	}

	if telephone.Valid {
		user.Telephone = telephone.String
	}

	if password.Valid {
		user.Password = password.String
	}
	return user, nil
}

func (us *UsersStorage) emailAvailable(email string) (bool, error) {
	row := Db.QueryRow(
		`select Count(*)
		from users 
		where email = ?`, email)

	var count int
	err := row.Scan(&count)

	switch {
	case err == sql.ErrNoRows:
		return true, nil
	case err != nil:
		return false, err
	}
	return count == 0, nil
}

func emailAvailableForUser(email string, user *User) (bool, error) {
	row := Db.QueryRow(
		`select Count(*)
		from users 
		where email = ?
		and id <> ?`, email, user.ID)

	var count int
	err := row.Scan(&count)

	switch {
	case err == sql.ErrNoRows:
		return true, nil
	case err != nil:
		return false, err
	}
	return count == 0, nil
}

// ErrUserNotFound - Error when the users isn't found
var ErrUserNotFound = errors.New("User Not Found")

// ErrEmailNotAvailable - Error when email to signup is already in use
var ErrEmailNotAvailable = errors.New("Email is already in use")

// ErrGoogleAccountCanChangeTheEmail - Error returned when a google user tries to change his email
var ErrGoogleAccountCanChangeTheEmail = errors.New("Email cannot be changed because this account is sync with google")

// ErrWrongPassword - wrong password authentication error
var ErrWrongPassword = errors.New("Password Provided doesn't match")

// ErrRecoverPasswordTokenNotFound - token not found in the database
var ErrRecoverPasswordTokenNotFound = errors.New("Recover password token not found in the database")
