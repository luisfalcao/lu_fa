package model

import (
	"database/sql"
	//I am creating a database connection here that uses this mysql driver
	_ "github.com/go-sql-driver/mysql"
)

// Db - Database object
var Db DB

// Connect - connect to database and return the connection
func Connect(host,
	port,
	user,
	password,
	database string) (db *sql.DB, err error) {
	db, err = sql.Open("mysql", user+":"+password+"@("+host+":"+port+")/"+database+"?parseTime=true")
	return
}

// DB interface of database
type DB interface {
	Exec(string, ...interface{}) (ExecResult, error)
	QueryRow(string, ...interface{}) Row
	Ping() error
}

// Row - interface to abstract QueryRow function result
type Row interface {
	Scan(...interface{}) error
}

// ExecResult - interface to abstract Exec function result
type ExecResult interface {
	LastInsertId() (int64, error)
	RowsAffected() (int64, error)
}

type sqlDB struct {
	db *sql.DB
}

func (s *sqlDB) Exec(query string, args ...interface{}) (ExecResult, error) {
	return s.db.Exec(query, args...)
}

func (s *sqlDB) QueryRow(query string, args ...interface{}) Row {
	return s.db.QueryRow(query, args...)
}

func (s *sqlDB) Ping() error {
	return s.db.Ping()
}

// SetDatabase - Stores the db connection
func SetDatabase(database *sql.DB) {
	Db = &sqlDB{database}
}
