package handlers

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strings"

	"bitbucket.org/luisfalcao/lu_fa/usersmanagement-api/apimodel"
	"bitbucket.org/luisfalcao/lu_fa/usersmanagement-api/model"
	"bitbucket.org/luisfalcao/lu_fa/usersmanagement-api/server/googlemodel"
	"github.com/rs/zerolog/log"
)

// GoogleSignin - creates the user from a google account
func GoogleSignin(client *http.Client,
	u *model.UsersStorage) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {

		authenticationHeader := r.Header.Get("Authorization")
		if len(authenticationHeader) == 0 {
			JSONResponse(w, "Authentication token not provided", http.StatusUnauthorized)
			return
		}
		authParts := strings.Split(authenticationHeader, " ")

		if authParts[0] != "OAuth2" {
			JSONResponse(w, "Invalid authentication schema", http.StatusUnauthorized)
			return
		}

		tokenInfoURL := fmt.Sprintf("https://oauth2.googleapis.com/tokeninfo?access_token=%s", authParts[1])
		res, err := client.Get(tokenInfoURL)
		if err != nil {
			log.Err(err).Caller().Msg("An error occurred trying to check the OAuth token from google")
			ErrorResponse(w)
			return
		}

		if res.StatusCode != http.StatusOK {
			validationResponse := apimodel.TokenRequestValidationError{
				Error:       "oauth_token_validation",
				Description: "The OAuth google token provided is not valid",
			}
			JSONResponse(w, validationResponse, http.StatusUnauthorized)
			return
		}

		var gUser googlemodel.GoogleUser

		if err := json.NewDecoder(res.Body).Decode(&gUser); err != nil {
			log.Err(err).Caller().Msg("An error occured trying to deserialize the google authentication response")
			JSONResponse(w, "An internal error occurred", http.StatusInternalServerError)
			return
		}

		user, err := u.GetUserByEmail(gUser.Email)
		var userID int64
		if err != nil {
			switch err {
			case model.ErrUserNotFound:
				userID, err = u.CreateUserFromGoogle(gUser.Email, "")
				if err != nil {
					JSONResponse(w, err.Error(), http.StatusBadRequest)
				}
			default:
				JSONResponse(w, err.Error(), http.StatusInternalServerError)
			}
		} else {
			userID = user.ID
		}

		if err != nil {
			switch err {
			case model.ErrEmailNotAvailable:
				validation := &apimodel.ValidationError{Error: err.Error()}
				JSONResponse(w, validation, http.StatusBadRequest)
			default:
				log.Err(err).Caller().Msg("Error occured creating the user from google.")
				ErrorResponse(w)
			}
			return
		}

		JSONResponse(w, apimodel.SignupResultModel{UserID: userID}, http.StatusCreated)

	}
}
