package handlers

import (
	"net/http"

	"bitbucket.org/luisfalcao/lu_fa/usersmanagement-api/apimodel"
	"bitbucket.org/luisfalcao/lu_fa/usersmanagement-api/authorization"
	"bitbucket.org/luisfalcao/lu_fa/usersmanagement-api/jwt"
	"bitbucket.org/luisfalcao/lu_fa/usersmanagement-api/model"
	"github.com/go-playground/validator/v10"
	schema "github.com/gorilla/Schema"
	"github.com/rs/zerolog/log"
)

// Authorize - function to generate tokens to the users
func Authorize(usersStorage *model.UsersStorage,
	validade *validator.Validate,
	decoder *schema.Decoder,
	c authorization.AuthorizedApplication) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {
		r.ParseForm()
		var tokenRequest apimodel.TokenRequest
		err := decoder.Decode(&tokenRequest, r.PostForm)
		if err != nil {
			log.Err(err).Caller().Msg("Error occurred trying to parse the TokenRequest.")
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		err = validade.Struct(tokenRequest)
		if err != nil {
			switch v := err.(type) {
			case validator.ValidationErrors:
				validationError := apimodel.NewValidationError(v)
				validationError.Error = "Some values wasn't provided or are in a wrong format"
				JSONResponse(w, validationError, http.StatusBadRequest)
			default:
				http.Error(w, err.Error(), http.StatusInternalServerError)
			}
			return
		}

		err = c.Validate(tokenRequest.ClientID, tokenRequest.ClientSecret, tokenRequest.GrandType)
		if err != nil {
			switch err.(type) {
			case authorization.AuthorizedApplicationInvalidError:
				vError := err.(authorization.AuthorizedApplicationInvalidError)
				validationResponse := apimodel.TokenRequestValidationError{
					Error:       string(vError.Type),
					Description: vError.Description,
				}
				JSONResponse(w, validationResponse, vError.HTTPStatusCode)
			default:
				log.Err(err).Caller().Msg("An error occurred validating the token.")
				ErrorResponse(w)
			}
			return
		}

		user, err := usersStorage.FindUserByEmailAndPassword(tokenRequest.Username, tokenRequest.Password)
		if err != nil {
			switch err {
			case model.ErrUserNotFound, model.ErrWrongPassword:
				validationResponse := apimodel.TokenRequestValidationError{
					Error:       authorization.InvalidGrant,
					Description: "Wrong email or password.",
				}
				JSONResponse(w, validationResponse, http.StatusForbidden)
			default:
				log.Err(err).Caller().Msg("An error occured trying to validate the user information")
				ErrorResponse(w)
			}
			return
		}

		token, err := jwt.CreateToken(user.ID)
		if err != nil {
			log.Err(err).Caller().Msg("Error creating the user token.")
			ErrorResponseWithBody(w, err.Error())
			return
		}

		tokenResult := apimodel.AuthorizedTokenResult{
			Token:     token.TokenValue,
			ExpiresIn: token.ExpiresIn,
			TokenType: "Bearer",
		}
		JSONResponse(w, tokenResult, http.StatusOK)
	}
}
