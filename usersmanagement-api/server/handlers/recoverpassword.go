package handlers

import (
	"encoding/json"
	"fmt"
	"net/http"
	"net/url"
	"time"

	"bitbucket.org/luisfalcao/lu_fa/usersmanagement-api/apimodel"
	"bitbucket.org/luisfalcao/lu_fa/usersmanagement-api/mail"
	"bitbucket.org/luisfalcao/lu_fa/usersmanagement-api/model"
	"github.com/go-playground/validator/v10"
	"github.com/rs/zerolog/log"
)

// RecoverPasswordRequest - endpoint to send mail with the recover password link
func RecoverPasswordRequest(m *mail.Mail,
	usersStorage *model.UsersStorage,
	validade *validator.Validate) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-type", "application/json")

		var requestModel apimodel.RecoverPasswordRequestModel

		if err := json.NewDecoder(r.Body).Decode(&requestModel); err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}

		err := validade.Struct(requestModel)
		if err != nil {
			switch v := err.(type) {
			case validator.ValidationErrors:
				validationError := apimodel.NewValidationError(v)
				validationError.Error = "The content of some fields are invalid"
				JSONResponse(w, validationError, http.StatusBadRequest)
			default:
				http.Error(w, err.Error(), http.StatusInternalServerError)
			}
			return
		}

		user, err := usersStorage.GetUserByEmail(requestModel.Email)
		if err != nil {
			switch err {
			case model.ErrUserNotFound:
				log.Info().Caller().Msgf("Failed to send recover password email because user with email '%s' was not found", requestModel.Email)
				BadRequest(w, apimodel.ValidationError{Error: "Not found any user with provided email"})
			default:
				log.Err(err).Caller().Msgf("Failed to send recover password for email '%s'.", requestModel.Email)
				ErrorResponse(w)
			}
			return
		}
		if user.GoogleSignUp {
			BadRequest(w, apimodel.ValidationError{Error: "Not possible to update the password of users synced with Google Account"})
			return
		}

		randToken := randToken()
		exp := time.Now().Add(time.Hour * 24)
		err = usersStorage.CreateRecoverPasswordToken(randToken, user.ID, exp)

		if err != nil {
			log.Err(err).Caller().Msgf("Failed to send recover password for email '%s'.", requestModel.Email)
			ErrorResponse(w)
			return
		}

		recoverURL, err := url.Parse(requestModel.RecoverURL)
		if err != nil {
			log.Err(err).Caller().Msgf("Failed to send recover password for email '%s'.", requestModel.Email)
			ErrorResponse(w)
			return
		}
		queryString := recoverURL.Query()
		queryString.Add("token", randToken)

		recoverURL.RawQuery = queryString.Encode()
		body := fmt.Sprintf(`
		<html>
			<body>
				<h3>
					Recover password
				<h3>
				<p>
					This password recover link was sent by the Management Users Application. 
					This link will only work just within the next 24 hours.
				</p>
				<p>
					<a href="%s" target="_blank">Recover Password</a>
				</p>
			</body>
		<body>
		`, recoverURL.String())

		message := mail.Message{
			From:    "notreplay@usersmanagement.com",
			To:      requestModel.Email,
			Subject: "Password recover",
			Message: body,
		}

		if err := m.Send(message); err != nil {
			log.Err(err).Caller().Msgf("Failed to send recover password for email '%s'.", requestModel.Email)
			ErrorResponse(w)
			return
		}
		w.WriteHeader(http.StatusOK)
	}
}
