package handlers

import (
	"net/http"
	"os"

	"bitbucket.org/luisfalcao/lu_fa/usersmanagement-api/model"
)

type dependencyStatus struct {
	Name                  string `json:"name"`
	DependencyIsReachable bool   `json:"dependencyIsReachable"`
	Error                 string `json:"error,omitempty"`
}

type status struct {
	Health       string             `json:"health"`
	Host         string             `json:"host"`
	Dependencies []dependencyStatus `json:"dependencies"`
}

// Healthcheck is a handler to return the status of the service
func Healthcheck(w http.ResponseWriter, r *http.Request) {

	w.Header().Set("Content-type", "application/json")
	host, _ := os.Hostname()

	var status = status{
		Health: "alive",
		Host:   host,
		Dependencies: []dependencyStatus{
			testDatabase(),
		},
	}
	JSONResponse(w, status, http.StatusOK)
}

func testDatabase() dependencyStatus {
	err := model.Db.Ping()
	databaseDependency := dependencyStatus{
		Name:                  "MySql Database",
		DependencyIsReachable: err == nil,
	}
	if err != nil {
		databaseDependency.Error = err.Error()
	}
	return databaseDependency
}
