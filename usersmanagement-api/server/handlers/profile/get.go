package profile

import (
	"errors"
	"net/http"

	"bitbucket.org/luisfalcao/lu_fa/usersmanagement-api/apimodel"
	"bitbucket.org/luisfalcao/lu_fa/usersmanagement-api/model"
	"bitbucket.org/luisfalcao/lu_fa/usersmanagement-api/server/handlers"
	"github.com/gorilla/context"
	"github.com/rs/zerolog/log"
)

// Get - handler that returns info about the logged user
func Get(usersStorage *model.UsersStorage) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-type", "application/json")

		userID, err := getUserID(r)
		if err != nil {
			log.Err(err).Caller().Msg("An error occured when trying to get user id from context.")
			handlers.ErrorResponseWithBody(w, "Internal server error")
		}

		user, err := usersStorage.FindUserByID(userID)
		if err != nil {
			log.Info().Int64("userID", userID).Msg("An error occured when trying to retrieve the user from the database.")
			handlers.ErrorResponseWithBody(w, "Internal server error")
			return
		}
		me := apimodel.ProfileResult{
			ID:          user.ID,
			Name:        user.Name,
			Address:     user.Address,
			Email:       user.Email,
			Telephone:   user.Telephone,
			CreatedDate: user.CreatedDate,
		}
		handlers.JSONResponse(w, me, http.StatusOK)
	}
}

func getUserID(r *http.Request) (int64, error) {
	if value := context.Get(r, "userID"); value != nil {
		return value.(int64), nil
	}
	return 0, errors.New("The userID is not present in the context")
}
