package profile

import (
	"encoding/json"
	"net/http"

	"bitbucket.org/luisfalcao/lu_fa/usersmanagement-api/apimodel"
	"bitbucket.org/luisfalcao/lu_fa/usersmanagement-api/model"
	"bitbucket.org/luisfalcao/lu_fa/usersmanagement-api/server/handlers"
	"github.com/go-playground/validator/v10"
	"github.com/rs/zerolog/log"
)

// Update - update the user with the data received in the request body
func Update(usersStorage *model.UsersStorage, validade *validator.Validate) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-type", "application/json")

		userID, err := getUserID(r)
		if err != nil {
			log.Err(err).Caller().Msg("An error occured when trying to get user id from context.")
			handlers.ErrorResponseWithBody(w, "Internal server error")
		}

		var requestModel apimodel.ProfileUpdateModel

		if err := json.NewDecoder(r.Body).Decode(&requestModel); err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}

		err = validade.Struct(requestModel)
		if err != nil {
			switch v := err.(type) {
			case validator.ValidationErrors:
				validationError := apimodel.NewValidationError(v)
				validationError.Error = "The content of some fields are invalid"
				handlers.JSONResponse(w, validationError, http.StatusBadRequest)
			default:
				http.Error(w, err.Error(), http.StatusInternalServerError)

			}
			return
		}

		user, err := usersStorage.Update(userID, requestModel.Email, requestModel.Name, requestModel.Telephone, requestModel.Address)
		if err != nil {
			switch err {
			case model.ErrEmailNotAvailable:
				handlers.BadRequest(w, apimodel.ValidationError{Error: "Email is already in use"})
			case model.ErrGoogleAccountCanChangeTheEmail:
				handlers.BadRequest(w, apimodel.ValidationError{Error: err.Error()})
			default:
				log.Err(err).Int64("userID", userID).Msg("An error occured when trying to update the user in the database.")
				handlers.ErrorResponseWithBody(w, "Internal server error")
			}
			return
		}

		me := apimodel.ProfileResult{
			Name:        user.Name,
			Address:     user.Address,
			Email:       user.Email,
			Telephone:   user.Telephone,
			CreatedDate: user.CreatedDate,
		}
		handlers.JSONResponse(w, me, http.StatusOK)
	}
}
