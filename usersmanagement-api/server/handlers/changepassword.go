package handlers

import (
	"crypto/rand"
	"encoding/base64"
	"encoding/json"
	"net/http"
	"time"

	"bitbucket.org/luisfalcao/lu_fa/usersmanagement-api/apimodel"
	"bitbucket.org/luisfalcao/lu_fa/usersmanagement-api/model"
	"github.com/go-playground/validator/v10"
	"github.com/rs/zerolog/log"
)

// ChangePassword -  endpoint to change password password
func ChangePassword(usersStorage *model.UsersStorage,
	validade *validator.Validate) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-type", "application/json")

		changePasswordModel := apimodel.ChangePasswordModel{}

		if err := json.NewDecoder(r.Body).Decode(&changePasswordModel); err != nil {
			log.Err(err).Caller().Msg("Failed to read as json the request body.")
			ErrorResponseWithBody(w, err.Error())
		}

		err := validade.Struct(changePasswordModel)
		if err != nil {
			switch v := err.(type) {
			case validator.ValidationErrors:
				validationError := apimodel.NewValidationError(v)
				validationError.Error = "The content of some fields are invalid"
				JSONResponse(w, validationError, http.StatusBadRequest)
			default:
				http.Error(w, err.Error(), http.StatusInternalServerError)
			}
			return
		}

		recoverPassToken, err := usersStorage.GetRecoverPassword(changePasswordModel.Token)
		if err != nil {
			log.Err(err).Caller().Msgf("Failed to change password with the provided token (Token: %s).", changePasswordModel.Token)
			BadRequest(w, apimodel.ValidationError{Error: "Was not possible to change password. The token provided was not valid."})
			return
		}

		exp := recoverPassToken.Expiration.Sub(time.Now()).Hours()

		if exp <= 0 {
			BadRequest(w, apimodel.ValidationError{Error: "Was not possible to change password. The token provided was not valid."})
		}

		user, err := usersStorage.GetUserByID(recoverPassToken.UserID)
		if err != nil {
			log.Err(err).Caller().Msgf("Failed to change password with the provided token (Token: %s).", changePasswordModel.Token)
			ErrorResponseWithBody(w, apimodel.ValidationError{Error: "Was not possible to change password.  An internal error occurred"})
			return
		}

		if err := usersStorage.UpdatePassword(changePasswordModel.Password, user.ID); err != nil {
			log.Err(err).Caller().Msgf("Failed to change password with the provided token (Token: %s).", changePasswordModel.Token)
			ErrorResponseWithBody(w, apimodel.ValidationError{Error: "Was not possible to change password. An internal error occurred"})
			return
		}

		_ = usersStorage.DeleteRecoverPassword(changePasswordModel.Token)

		w.WriteHeader(http.StatusOK)
	}

}

func randToken() string {
	b := make([]byte, 32)
	rand.Read(b)
	return base64.StdEncoding.EncodeToString(b)
}
