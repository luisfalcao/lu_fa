package handlers

import (
	"encoding/json"
	"net/http"

	"bitbucket.org/luisfalcao/lu_fa/usersmanagement-api/apimodel"
	"bitbucket.org/luisfalcao/lu_fa/usersmanagement-api/jwt"
	"bitbucket.org/luisfalcao/lu_fa/usersmanagement-api/model"
	"github.com/go-playground/validator/v10"
	"github.com/rs/zerolog/log"
)

// Signup is a handler to return the status of the service
func Signup(usersStorage *model.UsersStorage, validade *validator.Validate) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-type", "application/json")

		var signupModel apimodel.SignupModel

		if err := json.NewDecoder(r.Body).Decode(&signupModel); err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}

		err := validade.Struct(signupModel)
		if err != nil {
			switch v := err.(type) {
			case validator.ValidationErrors:
				validationError := apimodel.NewValidationError(v)
				validationError.Error = "The content of some fields are invalid"
				JSONResponse(w, validationError, http.StatusBadRequest)
				return
			default:
				log.Err(err).Caller().Msg("Error occured validating SignupModel.")
				ErrorResponseWithBody(w, err.Error())
				return
			}
		}

		userID, err := usersStorage.CreateUser(signupModel.Email, signupModel.Password)

		if err != nil {
			switch err {
			case model.ErrEmailNotAvailable:
				validation := &apimodel.ValidationError{Error: err.Error()}
				JSONResponse(w, validation, http.StatusBadRequest)
			default:
				log.Err(err).Caller().Msg("Error occured creating the user.")
				ErrorResponse(w)
			}
			return
		}

		token, err := jwt.CreateToken(userID)
		if err != nil {
			log.Err(err).Caller().Msg("Error creating the user token.")
			ErrorResponseWithBody(w, err.Error())
			return
		}

		JSONResponse(w, apimodel.SignupResultModel{UserID: userID, Token: token.TokenValue, ExpiresIn: token.ExpiresIn, TokenType: "Bearer"}, http.StatusCreated)
	}
}
