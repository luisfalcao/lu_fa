package server

import (
	"fmt"
	"net/http"
	"time"

	"github.com/rs/zerolog/log"
)

// StartServer function that configure and start to listening some port to receive http requests
func StartServer(wait chan bool, port int) *http.Server {

	addr := fmt.Sprintf(":%d", port)
	log.Info().Msgf("Staring http server on %s", addr)

	srv := &http.Server{
		Addr:         addr,
		Handler:      newRouter(),
		ReadTimeout:  time.Second * 5,
		WriteTimeout: time.Second * 10,
		IdleTimeout:  time.Second * 120,
	}

	go func() {
		log.Info().Msgf("Http server running on %s", addr)
		if err := srv.ListenAndServe(); err != nil {
			log.Error().Msgf("error running the server: %v", err)
			close(wait)
		}
	}()

	return srv
}
