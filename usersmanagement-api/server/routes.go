package server

import (
	"net/http"
	"os"
	"strconv"
	"time"

	"bitbucket.org/luisfalcao/lu_fa/usersmanagement-api/apimodel"
	"bitbucket.org/luisfalcao/lu_fa/usersmanagement-api/authorization"
	"bitbucket.org/luisfalcao/lu_fa/usersmanagement-api/mail"
	"bitbucket.org/luisfalcao/lu_fa/usersmanagement-api/model"
	"bitbucket.org/luisfalcao/lu_fa/usersmanagement-api/server/handlers"
	"bitbucket.org/luisfalcao/lu_fa/usersmanagement-api/server/handlers/profile"
	"bitbucket.org/luisfalcao/lu_fa/usersmanagement-api/server/middlewares"
	schema "github.com/gorilla/Schema"
	"github.com/gorilla/context"
	"github.com/gorilla/mux"
	"github.com/rs/zerolog/log"
)

func middleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		next.ServeHTTP(w, r)
	})
}

var (
	decoder      = schema.NewDecoder()
	usersStorage = new(model.UsersStorage)
	v            = apimodel.CreateValidator()
	client       = configureClient()
	mailClient   = createMailClient()
)

func newRouter() http.Handler {
	authorizedApp, err := authorization.CreateDefaultAuthorizedApplication()
	if err != nil {
		log.Err(err).Caller().Msg("Error occurred when trying to get the clients configuration")
	}

	mux := mux.NewRouter()

	mux.Use(middlewares.LogHandler)

	mux.Handle("/api/healthcheck", http.HandlerFunc(handlers.Healthcheck)).Methods("GET")

	mux.Handle("/api/profile", middlewares.AuthorizationMiddleware(client, usersStorage, profile.Get(usersStorage))).Methods("GET")
	mux.Handle("/api/profile", middlewares.AuthorizationMiddleware(client, usersStorage, profile.Update(usersStorage, v))).Methods("PUT")

	mux.Handle("/api/password/recover", handlers.RecoverPasswordRequest(mailClient, usersStorage, v)).Methods("POST")
	mux.Handle("/api/password/change", handlers.ChangePassword(usersStorage, v)).Methods("POST")

	mux.Handle("/connect/token", handlers.Authorize(usersStorage, v, decoder, authorizedApp)).Methods("POST")
	mux.Handle("/api/auth/signup", handlers.Signup(usersStorage, v)).Methods("POST")
	mux.Handle("/api/auth/goognesignin", handlers.GoogleSignin(client, usersStorage)).Methods("POST")

	http.Handle("/", context.ClearHandler(mux))

	return mux
}

func createMailClient() *mail.Mail {
	portStr := os.Getenv("SMTP_PORT")
	port, _ := strconv.Atoi(portStr)

	return mail.New(os.Getenv("SMTP_SERVER"),
		os.Getenv("SMTP_USER"),
		os.Getenv("SMTP_PASSWROD"),
		port)
}

func configureClient() *http.Client {
	return &http.Client{
		Transport: &http.Transport{
			MaxIdleConnsPerHost: 20,
		},
		Timeout: time.Duration(10) * time.Second,
	}
}
