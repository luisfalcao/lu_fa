package googlemodel

// GoogleUser - user data from google
type GoogleUser struct {
	Name  string `json:"name,omitempty"`
	Email string `json:"email"`
}
