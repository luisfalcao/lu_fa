package middlewares

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"net/http/httputil"
	"strings"

	"github.com/google/uuid"
	"github.com/rs/zerolog/log"
)

// LogHandler - log request and responses
func LogHandler(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		dumpBody := true
		if r.ContentLength > 1024 {
			dumpBody = false
		}
		dump, err := httputil.DumpRequest(r, dumpBody)
		if err != nil {
			http.Error(w, fmt.Sprint(err), http.StatusInternalServerError)
			return
		}

		reqID, err := uuid.NewRandom()
		if err != nil {
			http.Error(w, fmt.Sprint(err), http.StatusInternalServerError)
			return
		}

		log.Printf("<<<<< Request %s\n%s\n<<<<<", reqID.String(), string(dump))

		recorder := httptest.NewRecorder()
		defer func() {
			var sb strings.Builder
			fmt.Fprintf(&sb, "%s %d\n", recorder.Result().Proto, recorder.Result().StatusCode)

			for h, v := range recorder.Result().Header {
				w.Header()[h] = v
				for _, headerValue := range v {
					fmt.Fprintf(&sb, "%s: %s\n", h, headerValue)
				}
			}
			w.Header().Set("X-Request-Id", reqID.String())
			fmt.Fprintf(&sb, "X-Request-Id: %s\n", reqID.String())
			fmt.Fprintf(&sb, "Content-Length: %d\n", recorder.Body.Len())
			fmt.Fprint(&sb, "\n")
			sb.Write(recorder.Body.Bytes())

			log.Printf(">>>>> Response %s\n%s\n>>>>>", reqID.String(), sb.String())

			w.WriteHeader(recorder.Result().StatusCode)
			recorder.Body.WriteTo(w)
		}()
		next.ServeHTTP(recorder, r)
	})
}
