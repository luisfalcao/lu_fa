package middlewares

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strings"

	"bitbucket.org/luisfalcao/lu_fa/usersmanagement-api/apimodel"
	"bitbucket.org/luisfalcao/lu_fa/usersmanagement-api/jwt"
	"bitbucket.org/luisfalcao/lu_fa/usersmanagement-api/model"
	"bitbucket.org/luisfalcao/lu_fa/usersmanagement-api/server/googlemodel"
	"bitbucket.org/luisfalcao/lu_fa/usersmanagement-api/server/handlers"
	"github.com/gorilla/context"
	"github.com/rs/zerolog/log"
)

// AuthorizationMiddleware - middleware to verify if the request has the authorization token and if it's valid
func AuthorizationMiddleware(client *http.Client,
	u *model.UsersStorage,
	next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		authHeaderParts := strings.Split(r.Header.Get("Authorization"), " ")

		if len(authHeaderParts) < 2 {
			http.Error(w, "Authorization token was not provided or is in an invalid format", http.StatusUnauthorized)
			return
		}

		if authHeaderParts[0] == "OAuth2" {
			g, err := authorizedWithGoogle(client, w, r, authHeaderParts[1])
			if err != nil {
				http.Error(w, "Token not invalid or not provided", http.StatusUnauthorized)
				return
			}
			user, err := u.GetUserByEmail(g.Email)
			if err != nil {
				http.Error(w, "Authorization token was not provided or is in an invalid format", http.StatusUnauthorized)
				return
			}

			context.Set(r, "token", authHeaderParts[1])
			context.Set(r, "userID", user.ID)
			next.ServeHTTP(w, r)
		} else {
			userID, err := jwt.GetUserIDFromToken(authHeaderParts[1])
			if err != nil {
				switch err {
				case jwt.ErrTokenInvalid:
					http.Error(w, "Token not invalid or not provided", http.StatusUnauthorized)
				default:
					log.Err(err).Msg("An error occurred trying to validate the token")
					http.Error(w, "Internal server error", http.StatusInternalServerError)
				}
			} else {
				context.Set(r, "token", authHeaderParts[1])
				context.Set(r, "userID", userID)
				next.ServeHTTP(w, r)
			}
		}
	})
}

func authorizedWithGoogle(client *http.Client,
	w http.ResponseWriter,
	r *http.Request,
	token string) (*googlemodel.GoogleUser, error) {
	tokenInfoURL := fmt.Sprintf("https://oauth2.googleapis.com/tokeninfo?access_token=%s", token)
	res, err := client.Get(tokenInfoURL)
	if err != nil {
		log.Err(err).Caller().Msg("An error occurred trying to check the OAuth token from google")
		handlers.ErrorResponse(w)
		return nil, err
	}

	if res.StatusCode != http.StatusOK {
		validationResponse := apimodel.TokenRequestValidationError{
			Error:       "oauth_token_validation",
			Description: "The OAuth google token provided is not valid",
		}
		handlers.JSONResponse(w, validationResponse, http.StatusUnauthorized)
		return nil, err
	}

	gUser := googlemodel.GoogleUser{}

	if err := json.NewDecoder(res.Body).Decode(&gUser); err != nil {
		log.Err(err).Caller().Msg("An error occured trying to deserialize the google authentication response")
		handlers.JSONResponse(w, "An internal error occurred", http.StatusInternalServerError)
		return nil, err
	}

	return &gUser, nil
}
