package jwt

import (
	"errors"
	"os"
	"time"

	"github.com/dgrijalva/jwt-go"
	"github.com/rs/zerolog/log"
)

// ErrTokenInvalid - error when the token provided is invalid
var ErrTokenInvalid = errors.New("Token is invalid")

//ErrTokenExpired - error when the token provided is expired
var ErrTokenExpired = errors.New("Token is expired")

var signKey string

func init() {
	signKey = os.Getenv("SIGNKEY")
	if len(signKey) == 0 {
		log.Warn().Msg("SIGNKEY not set for JWT.")
	}
}

// CreateToken - this function creates a jwt token for a specific user
func CreateToken(id int64) (t *Token, err error) {
	t = &Token{}
	token := jwt.New(jwt.SigningMethodHS256)
	claims := make(jwt.MapClaims)

	now := time.Now()
	d := time.Hour * 24
	exp := now.Add(d)
	claims["exp"] = exp
	claims["iat"] = now
	claims["id"] = id

	token.Claims = claims

	tokenString, err := token.SignedString([]byte(signKey))
	if err == nil {
		t.TokenValue = tokenString
		t.ExpiresIn = d.Seconds()
	}
	return
}

// Token - the token structure
type Token struct {
	TokenValue string
	ExpiresIn  float64
}

func parseToken(val string) (id int64, err error) {
	token, err := jwt.Parse(val, func(token *jwt.Token) (interface{}, error) {
		return []byte(signKey), nil
	})

	switch err.(type) {
	case nil:
		if !token.Valid {
			return 0, ErrTokenInvalid
		}

		claims, ok := token.Claims.(jwt.MapClaims)
		if !ok {
			return 0, ErrTokenInvalid
		}
		var idCLaim = claims["id"]
		id = int64(idCLaim.(float64))

	case *jwt.ValidationError:
		vErr := err.(*jwt.ValidationError)

		switch vErr.Errors {
		case jwt.ValidationErrorExpired:
			return 0, ErrTokenExpired
		default:
			log.Err(err).Msgf("Token validation error")
			return 0, ErrTokenInvalid
		}
	}
	return
}

// GetUserIDFromToken - get user id from the provided token
func GetUserIDFromToken(token string) (userID int64, err error) {
	if len(token) == 0 {
		err = errors.New("Token provided in function GetUserIDFromToken is empty")
	}

	userID, err = parseToken(token)
	if err != nil {
		log.Err(err).Msgf("ParseToken error in the function GetUserIDFromToken.")
		err = ErrTokenInvalid
	}

	if userID < 0 {
		log.Err(err).Msgf("Important data is missing in the token")
		err = ErrTokenInvalid
	}

	return
}
